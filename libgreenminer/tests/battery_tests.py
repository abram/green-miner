#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import unittest
from unittest.mock import Mock
from ..phone import Battery, Phone

class FakeInfo(object):
	def __init__(self):
		self.status = Battery.BATTERY_HEALTH_GOOD
		self.charge = 100

	def read_battery_info(self):
		return """Current Battery Service state:
  AC powered: false
  USB powered: true
  Wireless powered: false
  status: {0}
  health: 2
  present: true
  level: {1}
  scale: 100
  voltage:4376
  temperature: 254
  technology: Li-ion""".format(self.status, self.charge)

class BatteryTests(unittest.TestCase):
	def setUp(self):
		self.fake_info = FakeInfo()
		self.phone = Phone("DUMMY_SERIAL")
		self.phone.battery.read_battery_info = Mock(return_value=self.fake_info.read_battery_info())

	def test_parses_level(self):
		"""Parse Battery Level"""
		for i in range(101):
			self.fake_info.charge = i
			# Make our "phone" show updated charge info
			self.phone.battery.read_battery_info.return_value = self.fake_info.read_battery_info()
			self.assertEqual(self.phone.battery.get_charge_level(), i)

	def test_parses_status(self):
		"""Parse Battery Status"""
		valid_statuses = [Battery.BATTERY_UNKNOWN, Battery.BATTERY_CHARGING,
			Battery.BATTERY_DISCHARGING, Battery.BATTERY_NOT_CHARGING,
			Battery.BATTERY_FULL]
		for i in valid_statuses:
			self.fake_info.status = i
			# Make our "phone" show updated status info
			self.phone.battery.read_battery_info.return_value = self.fake_info.read_battery_info()
			self.assertEqual(self.phone.battery.get_status(), i)

	def test_raise_exception(self):
		"""Exceptions Raised from bad battery information"""
		# ADB Outputs "error: device not found" to STDERR (not STDOUT!)
		# It returns no output in the case of nothing present

		self.phone.battery.read_battery_info.return_value = None
		with self.assertRaises(Exception):
			self.phone.battery.get_charge_level()
			self.phone.battery.get_health()
			self.phone.battery.get_status()
			self.phone.battery.get_temperature()

		self.phone.battery.read_battery_info.return_value = ""
		with self.assertRaises(Exception):
			self.phone.battery.get_charge_level()
			self.phone.battery.get_health()
			self.phone.battery.get_status()
			self.phone.battery.get_temperature()

if __name__ == '__main__':
	unittest.main()
