#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import io, unittest
from unittest.mock import Mock
from ..devicebuilder import DeviceBuilder

class FakeInfo(object):
	def __init__(self):
		self.deviceid = "T"
		self.serial = "DUMMY_SERIAL"
		self.config = "{{\"{0}\":{{\"port\":\"/fake/Port0\",\"serial\":\"{1}\",\"battery\":false,\"tuning\":{{\"type\":\"ina219\",\"date\":\"2013-06-26\"}}}}}}".format(self.deviceid, self.serial)

class DeviceBuilderTests(unittest.TestCase):
	def setUp(self):
		self.info = FakeInfo()
		self.config = io.StringIO(self.info.config)

	def test_find_by_serial(self):
		"""Device Builder with Serial"""
		device_builder = DeviceBuilder(devicefile=self.config, serial=self.info.serial)
		phone = device_builder.build_phone()
		self.assertEqual(self.info.serial, phone.serial)

	def test_find_by_device_id(self):
		"""Device Builder with Device ID"""
		device_builder = DeviceBuilder(devicefile=self.config, deviceid=self.info.deviceid)
		phone = device_builder.build_phone()
		self.assertEqual(self.info.serial, phone.serial)
