# Copyright (c) 2014 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import textwrap, unittest
from unittest.mock import Mock

from nose.tools import *
from nose.plugins.attrib import attr

from libgreenminer import Queue

apps = list('abcdefghijklmn')
versions = range(1, 10)
app_versions = [(app, version) for app in apps for version in versions]
tests = ['one', 'two', 'three']
batches = ['hamburger', 'palace', 'forever']

# fake queue with 3 tests, 3 batches
# each app:version has 4 tests in each of
# one#hamburger, two#palace, three#forever
fake_queue_data = {
        '{}:{}'.format(app, version): {
            '{}#{}'.format(test, batch): 4
            for test, batch in zip(tests, batches)
         }
        for app in apps for version in versions
}

class QueueTests(unittest.TestCase):
        def setUp(self):
            self.queue = Queue('android')
            self.queue.set_data(fake_queue_data)

        @attr(unit=True)
        def test_runs_for_batch(self):
            expected = len(app_versions) * 4
            for batch in batches:
                eq_(self.queue.get_runs_for_batch(batch), expected)

        @attr(unit=True)
        def test_runs_for_app_version(self):
            expected = len(batches) * 4
            for app, version in app_versions:
                eq_(self.queue.get_app_ver_total_runs(app, version), expected)

        @attr(unit=True)
        def test_get_test_runs_for_app_version(self):
            for app, version in app_versions:
                test_runs = self.queue.get_runs_for_tests(app, version)
                eq_(set(tests), set(test_runs.keys()))

                for count in test_runs.values():
                    eq_(count, 4)

        @attr(unit=True)
        def test_get_total_runs(self):
            eq_(self.queue.get_total_runs(),
                len(app_versions) * 4 * len(batches))


if __name__ == '__main__':
	unittest.main()

