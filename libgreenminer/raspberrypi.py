#
# Copyright (c) 2014 Wyatt Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import calendar, shlex, subprocess, time, os, json

from .device import Device
from .test import TestFile, RaspberryPiTest


class RaspberryPi(Device):
    def __init__(self, username, ip_address, sleep=time.sleep):
        self.username = username
        self.ip_address = ip_address
        self.host_string = "{}@{}".format(username, ip_address)

        self.sleep = sleep
        self.dir_prefix = []

    def build_test(self, test_file):
        """Builds a test from the test_file"""
        return RaspberryPiTest(test_file)

    # abstracted
    def shell(self, command, async=False):
        """Shell to SSH interface"""
        return self.ssh(command, async)

    def sudo(self, command, async=False):
        """Alias to ssh"""
        return self.ssh("sudo " + command, async)

    def ssh(self, command, async=False):
        """Executes a SSH command on the RaspberryPi"""

        # Add on dir prefixes
        if len(self.dir_prefix) >= 0:
            command = ' && '.join(self.dir_prefix + [command])

        # Extract the arguments
        arguments = shlex.split('"' + command + '"')
        command_list = [ 'ssh', '-t', self.host_string ]
        command_list += arguments

        try:
            # If async, just fire this off
            if async:
                # print("ASYNC SSH Command: " + str(command_list))
                return subprocess.Popen(command_list)

            # print("SSH Command: " + str(command_list))

            # Else, return the output
            return subprocess.check_output(command_list).decode('UTF-8')
        except subprocess.CalledProcessError:
            import datetime
            import os
            print(os.getpid())
            print(str(datetime.datetime.now()))
            raise Exception("Failed to execute: " + str(command))

    def push(self, src, target, many=False):
        """Push a file (src) to a location (target) on the phone."""
        try:
            command_list = ['rsync', '-aP', src, self.host_string + ":" + target]

            # print("SCP Command: " + str(command_list))

            return subprocess.check_output(command_list).decode('UTF-8')
        except subprocess.CalledProcessError:
            raise Exception("Failed to put " + str(src) + " -> " + str(target))

    def rm(self, src):
        """Removes a folder/file from the pi"""
        self.sudo("rm -rf " + src)

    def dump_info(self):
        """Returns a dict with lots of info about the phone and
            the current environment."""

        return {
            'time': int(calendar.timegm(time.gmtime())),
            'os_version': self.shell('cat /etc/issue.net').strip()
        }

    def cd(self, directory):
        return RaspberryPi.CDObject(self, directory)

    class CDObject(object):
        def __init__(self, raspberrypi, directory):
            self.raspberrypi = raspberrypi
            self.directory = directory

        def __enter__(self):
            # Build the new dir command
            new_dir = 'cd ' + self.directory

            # Add it
            self.raspberrypi.dir_prefix.append(new_dir)

        def __exit__(self, exc_type, exc_value, traceback):
            # Reset the old path
            self.raspberrypi.dir_prefix.pop()

    def find_tarball(self, app, commit):
        config = json.loads(open('config.json').read())
        TARBALL_FOLDER = os.path.expanduser(config['tarballs_folder'])

        for d in [os.path.join(TARBALL_FOLDER, app), TARBALL_FOLDER]:
            try:
                for f in os.listdir(d):
                    if(f == commit + '.tar.bz2'):
                        return os.path.join(d, f)
            except FileNotFoundError:
                pass

        raise FileNotFoundError("No tarball found in " + str(TARBALL_FOLDER))

    def install_context(self, args):
        return RaspberryPi.Packages(self, args)

    class Packages(object):
        def __init__(self, raspberrypi, args):
            self.tarball = raspberrypi.find_tarball(args.app, args.version)
            self.raspberrypi = raspberrypi
            self.args = args

        def __enter__(self):
            # Setup source files to test with
            test_dir = os.path.join('tests', self.args.test)
            app_dir = os.path.join(test_dir, self.args.app)
            files_dir = os.path.join(app_dir, 'files')

            # Create the temp folder on the pi
            self.raspberrypi.ssh("rm -rf /tmp/rpi")
            self.raspberrypi.ssh("mkdir -p /tmp/rpi")

            # Check if the folder exists at all
            if os.path.exists(files_dir):
                # As it does, lets check if it actually has files in it
                if len(os.listdir(files_dir)) > 0:
                    # It does have files! Lets push it!
                    self.raspberrypi.push(files_dir, '/tmp/rpi/', many=True)

            # Push the app bundle
            self.raspberrypi.push(self.tarball, '/tmp/rpi/tarball.tar.bz2')

            # Perform extraction and clean up
            with self.raspberrypi.cd("/tmp/rpi"):
                self.raspberrypi.ssh("tar xjf tarball.tar.bz2")
                self.raspberrypi.ssh("rm tarball.tar.bz2")

            # Get installed packages
            self.packages = self.raspberrypi.ssh("dpkg --get-selections | grep -v deinstall")
            self.packages = [ y for y in [x.split("\t")[0] for x in self.packages.split("\r\n")] if len(y) != 0 ]

            return self

        def __exit__(self, exc_type, exc_value, traceback):
            # Remove the source
            self.raspberrypi.ssh('rm -rf /tmp/rpi')
