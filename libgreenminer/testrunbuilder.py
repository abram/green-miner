#
# Copyright (c) 2014 Wyatt Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from .phone import Phone
from .raspberrypi import RaspberryPi

from .test import AndroidTestRun, RaspberryPiTestRun


class TestRunBuilder(object):
    """"""

    def __init__(self, device, **kwargs):
        self.device = device
        self.kwargs = kwargs

    def build_testrun(self):
        if isinstance(self.device, Phone):
            return AndroidTestRun(
                phone=self.device,
                arduino=self.kwargs['arduino'],
                app=self.kwargs['app'],
                version=self.kwargs['version'],
                batch=self.kwargs['batch'],
                test=self.kwargs['test'],
                name=self.kwargs['name'],
                packages=self.kwargs['packages']
            )
        elif isinstance(self.device, RaspberryPi):
            return RaspberryPiTestRun(
                raspberrypi=self.device,
                arduino=self.kwargs['arduino'],
                app=self.kwargs['app'],
                version=self.kwargs['version'],
                batch=self.kwargs['batch'],
                test=self.kwargs['test'],
                name=self.kwargs['name'],
                packages=self.kwargs['packages']
            )
