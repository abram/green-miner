

from .test import RaspberryPiTest, AndroidTest


class TestBuilder(object):
    """Builds a test from a device"""

    def __init__(self, device):
        self.device = device

    def build_test(self, test_file):
        return self.device.build_test(test_file)
