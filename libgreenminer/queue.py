#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import json, random, requests, sys, time

from itertools import *
from operator import itemgetter

from .phone import Phone
from .raspberrypi import RaspberryPi


config = json.loads(open('config.json').read())

NONE_LEFT_RETRY = 60;


def inverted_dict_zipmap(func, d):
    """helper method for mapping a dict"""
    for key, values in d.items():
        for v in func(values):
            yield (v, key)

def batches_for_entry(entry):
    return sorted(set(map(get_batch, entry.keys())))

def get_batch(test_batch):
    return test_batch.split('#')[1]

def get_test(test_batch):
    return test_batch.split('#')[0]


class Queue(object):
    ''' handles the web queue.

        Instantiate with either a string describing the device type or
        a device object (as built by DeviceBuilder).

        queue format is:
        {
            '<app>:<version>': {
                '<test>#<batch>': <count>
            }
        }

        for as many app/version/test/batch combos as required
    '''
    def __init__(self, device):
        if device == 'raspberrypi' or isinstance(device, RaspberryPi):
            self.url = config['tarball_queue_url']
        elif device == 'android' or isinstance(device, Phone):
            self.url = config['queue_url']
        else:
            raise Exception("Unknown device: " + device)

    def pull(self):
        self.set_data(requests.get(self.url + '?show_queue', verify=False).json())

    def set_data(self, data):
        self.data = data
        # mame a list of (apk, batch) tuples
        apk_batches = sorted(inverted_dict_zipmap(batches_for_entry,
                            self.data))

        # turn it into batch -> [apk]
        self.apks_per_batch = dict( (batch, [apk for b, apk in batch_apk]) \
            for batch, batch_apk in groupby(apk_batches, itemgetter(0)))

        self.batches = list(self.apks_per_batch.keys())

    def get_app_versions(self):
        '''return an iterator of (app, version) pairs'''
        return map(lambda x: x.split(':'), self.data.keys())

    def get_runs_for_batch(self, batch):
        return sum(self.get_test_runs_for_batch(batch).values())

    def get_test_runs_for_batch(self, batch):
        test_runs = {}

        for app_version, tests in self.data.items():
            for test_batch, count in tests.items():
                test, b = test_batch.split('#')
                if b == batch:
                    test_runs[test] = test_runs.get(test, 0) + count
        return test_runs

    def get_runs_per_batch(self):
        runs = {}

        for app_version, tests in self.data.items():
            for test_batch, count in tests.items():
                batch = get_batch(test_batch)
                runs[batch] = runs.get(batch, 0) + count
        return runs
            
    def get_runs_for_tests(self, app, version):
        # '<app>:<version>': {
        #       '<test>#<batch>': <count>
        #   }
        app_ver_key = '{}:{}'.format(app, version)

        counts = {}
        for test_batch, count in self.data.get(app_ver_key, {}).items():
            test = get_test(test_batch)
            counts[test] = counts.get(test, 0) + count
        return counts

    def get_app_ver_total_runs(self, app, version):
        return sum(self.get_runs_for_tests(app, version).values())

    def get_total_runs(self):
        return sum(starmap(self.get_app_ver_total_runs,
                    self.get_app_versions()))

    def queue_waiter(self):
        """creates an iterator that polls the queue at most once per
            NONE_LEFT_RETRY seconds.
            yields False when it is empty
            yields True when it is done
        """
        last_check = time.time()

        while True:
            try:
                self.pull()
                if not self.is_empty():
                    break
                last_check = time.time()
                msg = "No commits in queue, refresh in {} seconds"
            except requests.exceptions.RequestException:
                msg = "Error acessing remote queue, retrying in {} seconds"
            except ValueError:
                msg = "Failed to read JSON from queue, retrying in {} seconds"

            print(msg.format(NONE_LEFT_RETRY), file=sys.stderr)
            yield False
            time_until_check = (last_check + NONE_LEFT_RETRY) - time.time()
            if time_until_check > 0:
                time.sleep(time_until_check)

        yield True

    def wait_for_queue(self):
        ''' simple wrapper around queue_waiter that does nothing
            useful'''
        for check in self.queue_waiter():
            pass

    def is_empty(self):
        return len(self.data) == 0

    def get_random_test_from_batch(self, batch):
        ''' returns app, version, test '''
        if not batch in self.apks_per_batch:
            return (None, None, None)

        app_version = random.choice(self.apks_per_batch[batch])
        tests = list(filter(lambda test: get_batch(test) == batch,
                    self.data[app_version].keys()))
        app, version = app_version.split(':')
        return app, version, random.choice(tests).split('#')[0]

    def report_finished(self, test, app, version, batch):
        '''report a completed test run to the queue, but don't update our data'''
        report_me = json.dumps([':'.join([app, version]),
            '#'.join([test, batch])]).encode('ASCII')
        while True:
            try:
                result = requests.post(self.url + '?report_finished',
                        data=report_me, timeout=1, verify=False,
                        headers={'content-type': 'application/json'})
                print('{}: {}'.format(result.status_code, result.text))
                return
            except requests.exceptions.RequestException:
                print("Error reporting finished test, retrying in 5 seconds",
                    file=sys.stderr)
                time.sleep(5)
