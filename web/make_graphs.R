#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
library(ggplot2)

cbPalette <- c("#1F78B4", "#33A02C", "#E31A1C", "#FF7F00", "#6A3D9A", "#B15928", "#A6CEE3", "#B2DF8A", "#FB9A99", "#FDBF6F", "#CAB2D6", "#FFFF99")

plotpdf <- function(pdfname, f, width=12, height=12) {
  pdf(pdfname,width=width,height=height)
  f()
  dev.off()
}

###########################################
# function definitions					  #
###########################################
polyplot <- function(dists,names,ylab="Y",xlab="X",main="") {
  mindist <- min(sapply(dists,min))
  maxdist <- max(sapply(dists,max))
  plot(c(),xlim=c(1,length(dists)),ylim=c(mindist,maxdist),xaxt="n",ylab=ylab,xlab=xlab,main=main)
  w <- c()
  w$x <- c(1:length(dists))
  w$x <- c(w$x,rev(w$x)) # from left to right, turn around right to left
  w$y <- c(sapply(dists,min),rev(sapply(dists,max)))
  # lty=2 dashed
  polygon(w$x,w$y,col="lightgreen",border="green",lty=1,fillOddEven=T)
  # LINES?
  lines(c(1:length(dists)),sapply(dists,mean),col="red",lw=4,lty=5)
  boxplot(dists,add=1,col="white",names=names)
  #axis(below,c(1:5,10,20,30),c("One","Two","Tree","Four","Five","Ten","Twenty","Thirty"))

}

labelmatrixmap <- function(m,f) {
  lrow = labels(m)[[1]]
  lcol = labels(m)[[2]]
  ol =
    sapply(1:length(lcol), function(col) {
      sapply(1:length(lrow), function(row) {
      f(row,col,lrow[row],lcol[col],m[row,col])
    })
  })
  matrix(data=ol,nrow=length(lrow),ncol=length(lcol), dimnames=labels(m))
}
matrixmap <- function(m,f) {
  ol =
    sapply(1:ncol(m), function(col) {
      sapply(1:nrow(m), function(row) {
      f(row,col,m[row,col])
    })
  })
  matrix(data=ol,nrow=nrow(m),ncol=ncol(m), dimnames=labels(m))
}
matrixToImageLoc <- function(m) {
  xm <- matrixmap(m, function(x,y,val) {
    x
  })
  ym <- matrixmap(m, function(x,y,val) {
    y
  })
  list(xm,ym)
}
textOnImageMatrix <- function(textmatrix,col="black") {
  l <- matrixToImageLoc(textmatrix)
  xm <- l[[1]]
  ym <- l[[2]]
  text((xm-1.0)/(nrow(textmatrix)-1),(ym-1.0)/(ncol(textmatrix)-1),textmatrix,col=col)
}

make_dists <- function(data, column) { lapply(app_versions, function(app_v){
	data[data$app_v==app_v,column]
})}

make_app_v <- function(data) {
	app_v <- apply(data, 1, function(row) {
		paste(row[["app"]], row[["version"]], sep='\n')
	})

	# maintain the original order of the data
	factor(app_v, levels=unique(app_v))
}

############################################
# read and set up data					   #
############################################
# totals
totals <- read.csv("totals.csv",header=T)
totals$version <- factor(totals$version, levels=unique(totals$version))

totals$app_v <- make_app_v(totals)
test_label = paste(unique(totals$Test), collapse=' - ')

# partitions
partitions <- read.csv('partitions.csv', header=T,
	colClasses=c("version"="character", "joules"="numeric",
				 "start"="numeric"))
partitions$version <- factor(partitions$version,
						levels=unique(partitions$version))

partition_info <- read.csv('partition_info.csv', header=T)
partition_info$name <- as.character(partition_info$name)


# join partition_info name column into name column of partitions
partitions$name <- Reduce(c, lapply(unique(partitions$run), function(run) {
	  test <- partitions[partitions$run==run, 'test'][[1]]
	  partition_info[partition_info$test==test, 'name']
}))

partitions$name <- factor(partitions$name,
	levels=unique(partition_info$name))

# make sure joules rows are numeric, not factors
partitions$joules <- as.numeric(as.character(partitions$joules))
partitions$start <- as.numeric(as.character(partitions$start))
partitions$app_v <- make_app_v(partitions)

# unique apps + versions
app_versions <- unique(totals$app_v)

# unique device ids in order
devices <- unique(partitions$device)
devices <- devices[order(devices)]


# load and apply models
models <- read.csv('models.csv', header=T)
models$intercept <- as.numeric(as.character(models$intercept))
models$slope <- as.numeric(as.character(models$slope))

for (measure in unique(models$measure)) {
	device_models <- models[models$measure==measure,]

	partitions[,measure] <- apply(partitions, 1, function(row) {
		model <- device_models[device_models$device==row[['device']],]
		as.numeric(row[[measure]]) * model[['slope']] + model[['intercept']]
	})

	totals[,measure] <- apply(totals, 1, function(row) {
		model <- device_models[device_models$device==row[['device']],]
		as.numeric(row[[measure]]) * model[['slope']] + model[['intercept']]
	})
}


###############################################
# do actual graphing now					  #
###############################################


# do joules per-revision boxplot
plotpdf("joules.pdf", function() {
  polyplot(make_dists(totals, "joules"), app_versions, main=test_label,
		  	xlab="App and Version", ylab="Joules")
})

# do meanwatt per-revision boxplot
plotpdf("meanwatts.pdf", function() {
  polyplot(make_dists(totals, "meanwatts"), app_versions, main=test_label,
		   xlab="App and Version",ylab="Mean Watt")

  for (app_v in app_versions) {
	data <- totals[totals$app_v == app_v, ]
  	print(ggplot(data, aes(x=device, y=meanwatts)) + geom_boxplot()
		+ xlab("App Versions") + ylab("Length (seconds)")
		+ ggtitle(test_label))
  }
})

# do tests per-revision bar graph
plotpdf("run-counts.pdf", function() {
  print(ggplot(totals, aes(x=factor(app_v)))
  	+ geom_bar()
  	+ geom_bar(aes(fill=device), position='dodge')

  	+ xlab("App Versions")
	+ ylab("Test Runs")
	+ ggtitle(test_label))
})



# do timing per-revision, per-name boxplots
plotpdf('component-timing.pdf', function() {
	print(ggplot(partitions, aes(x=name, y=duration, color=app_v)) + geom_boxplot() + xlab("App Versions") + ylab("Length (seconds)") + ggtitle(test_label))
})

# do joules per-revision, per-component boxplots
plotpdf("component-joules.pdf", function() {
	print(ggplot(partitions, aes(x=name, y=joules, color=app_v)) + geom_boxplot() + xlab("App Versions") + ylab("Joules") + ggtitle(test_label))
})

# do meanwatts per-revision, per-component boxplots
plotpdf("component-meanwatts.pdf", function() {
	print(ggplot(partitions, aes(x=name, y=meanwatts, color=app_v)) + geom_boxplot() + xlab("App Versions") + ylab("Mean Watts") + ggtitle(test_label))
})


components <- unique(partitions$name)

# do timing per-revision, per-name/device, boxplots
plotpdf('component-dev-timing.pdf', function() {
	for (c in components) {
		data <- partitions[partitions$name==c,]
		print(ggplot(data, aes(x=device, y=duration, color=app_v))
			  + geom_boxplot() + xlab("App Versions")
			  + ylab("Length (seconds)")
			  + ggtitle(paste(c, "duration")))
	}
})

# do joules per-revision, per-component/device boxplots
if (length(partitions$meanamps) > 0) {
	plotpdf('component-dev-meanamps.pdf', function() {
		for (c in components) {
			data <- partitions[partitions$name==c,]
			print(ggplot(data, aes(x=device, y=meanamps, color=app_v))
				  + geom_boxplot() + xlab("App Versions")
				  + ylab("Mean Milliamps")
				  + ggtitle(paste(c, "Mean Milliamps")))
		}
	})
}

# do joules per-revision, per-component/device boxplots
plotpdf('component-dev-joules.pdf', function() {
	for (c in components) {
		data <- partitions[partitions$name==c,]
		print(ggplot(data, aes(x=device, y=joules, color=app_v))
			  + geom_boxplot() + xlab("App Versions")
			  + ylab("Joules")
			  + ggtitle(paste(c, "Joules")))
	}
})

# do meanwatts per-revision, per-component/device boxplots
plotpdf('component-dev-meanwatts.pdf', function() {
	for (c in components) {
		data <- partitions[partitions$name==c,]
		print(ggplot(data, aes(x=device, y=meanwatts, color=app_v))
			  + geom_boxplot() + xlab("App Versions")
			  + ylab("Mean Watts")
			  + ggtitle(paste(c, "Mean Watts")))
	}
})



######################################
# stacked graphs					 #
######################################

# take the mean of numeric columns, or the first value of
# string columns (any string columns should be the same in each
# group anyway).
stacked <- aggregate(partitions, by=list(partitions$app_v, partitions$name, partitions$test), FUN=function(x) {
	if (is.numeric(x)) {
		mean(x)
	} else {
		x[[1]]
	}
})

grouping <- factor(stacked$app_v)
plotpdf('stacked.pdf', function() {
	xlabel <- "revision"
	title <- "mean joules per component"

	# stacked bar graph of cumulative watt seconds
	print(ggplot(stacked, aes(x=grouping, y=joules, fill=name)) + geom_bar(stat="identity") + scale_fill_manual(values=cbPalette) + coord_flip() + xlab(xlabel) + ggtitle(title))

	# stacked bar graph of proportional watt seconds
	print(ggplot(stacked, aes(x=grouping, y=joules, fill=name)) + geom_bar(stat="identity", position="fill") + scale_fill_manual(values=cbPalette) + coord_flip() + xlab(xlabel) + ggtitle(title))

	# grouped bar graph of cumulative watt seconds
	print(ggplot(stacked, aes(x=grouping, y=joules, fill=name)) + geom_bar(position="dodge", stat="identity") + scale_fill_manual(values=cbPalette) + coord_flip() + xlab(xlabel) + ggtitle(title))
})

# do over-time, per-component joules
# note: if two batches are run at very different times, there will
# be lots of empty space in the graph
plotpdf('joules_over_time_per_app.pdf', function() {
	for (app_v in app_versions) {
		data <- partitions[partitions$app_v==app_v,]
		min_time <- min(data$start)
		max_time <- max(data$start)

		max_joules <- max(data$joules)
		min_joules <- min(data$joules)

		print(ggplot(data, aes(x=start, y=joules, colour=name))
			+ geom_point(stat="identity")
			+ scale_fill_manual(values=cbPalette)
			+ ggtitle(paste(app_v, " over time"))
			+ coord_cartesian(xlim=c(min_time, max_time),
					ylim=c(min_joules, max_joules)))
	}
})


# do over-time, per-component joules
# note: if two batches are run at very different times, there will
# be lots of empty space in the graph
plotpdf('joules_over_time_per_app-device.pdf', function() {
	for (dev in devices) {
		for (app_v in app_versions) {
			data <- partitions[partitions$app_v==app_v & partitions$device==dev,]

			if (nrow(data) < 1) {next}

			min_time <- min(data$start)
			max_time <- max(data$start)

			max_joules <- max(data$joules)
			min_joules <- min(data$joules)

			print(ggplot(data, aes(x=start, y=joules, colour=name))
				+ geom_point(stat="identity")
				+ scale_fill_manual(values=cbPalette)
				+ ggtitle(paste(app_v, " over time on ", dev))
				+ coord_cartesian(xlim=c(min_time, max_time),
						ylim=c(min_joules, max_joules)))
		}
	}
})


# do pairwise t-test for similarity in per-revision  joules
# (as long as there are at least 2 app versions tested)
if (length(app_versions) >= 2) {
	len <- length(totals$joules)
	si <- c()
	totals_sample <- totals
	if (length(totals$joules) < 140) {
	  si <- sample(1:len,max(140,length(totals$joules)),replace=TRUE)
	  totals_sample <- totals[si,]
	}
	totals_sample <- totals_sample[order(totals_sample$app_v),]
	ptt <- pairwise.t.test(totals_sample$joules, totals_sample$app_v,p.adjust="bonf")#,p.padjust="none")
	#ptt <- pairwise.t.test(totals_sample$joules, unique(totals_sample$Commits))
	plotpdf("joules-similarity.pdf", function() {
	  labelx <- labels(ptt$p.value)[[1]]
	  labely <- labels(ptt$p.value)[[2]]
	  #labelx <- sapply(labels(ptt$p.value)[[1]],app_v)
	  #labely <- sapply(labels(ptt$p.value)[[1]],app_v)
	  image(ptt$p.value < 0.05,xaxt="n",yaxt="n",col=c("#000000","#CCCCCC"))
	  axis(below<-1,at=seq(0,1,l=ncol(ptt$p.value)), labels = labelx)
	  axis(side<-2,at=seq(0,1,l=nrow(ptt$p.value)), labels = labely)
	  textOnImageMatrix(
	    labelmatrixmap(ptt$p.value, function(row,col,rowl,coll,va) {
	      round(mean(totals_sample$joules[totals_sample$app_v == rowl]) - mean(totals_sample$joules[totals_sample$app_v == coll]) , digits=3)
	    }), col="red"
	    )
	})
}
