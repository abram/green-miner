#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import cgi, csv, subprocess, os, sys, tarfile, tempfile
import cgitb

cgitb.enable()

GNUPLOT = 'gnuplot'
WEBDAV_FOLDER = 'uploads/data'

# Get Arguments from URL (?var1=val1&var2=val2)
arguments = cgi.FieldStorage()

# Check for file argument
if("file" not in arguments):
	print("Content-type: text/plain\n")
	print("Please specify a file to open.")
	sys.exit()
else:
	filename = arguments["file"].value

# Formats for terminal output
def write2pipe(input_type, string):
	string = string + '\n'
	string = string.encode('utf-8')
	input_type.write(string)

# Open GNUPlot
plot = subprocess.Popen([GNUPLOT,'-p'], stdin=subprocess.PIPE)

# Pretty Settings
title = filename.replace('.tar.gz','')
if("png" in arguments):
	write2pipe(plot.stdin, "set terminal pngcairo size 800,400 font 'inconsolata, 8'".format(title.replace('.','_')))
else:
	write2pipe(plot.stdin, "set terminal svg size 800,400 name '{0}' font 'inconsolata, 8'".format(title.replace('.','_')))

write2pipe(plot.stdin, "load \"resources/color.plt\"") # Load style

write2pipe(plot.stdin, "set border 3 back ls 13")
write2pipe(plot.stdin, "set grid back ls 14")
write2pipe(plot.stdin, "set tics nomirror") # Tics only on bottom and left
write2pipe(plot.stdin, "unset key") # Remove Filename
write2pipe(plot.stdin, "set obj 1 rect from graph 0, graph 0 to graph 1, graph 1 fs solid 1 fc rgb '#FFFFFF' behind lw 0") # White BG
# Output to temp file
(fd, tempfilename) = tempfile.mkstemp()
write2pipe(plot.stdin, "set output '{0}'\n".format(tempfilename))

# Labels
write2pipe(plot.stdin, "set title '{0}' font 'inconsolata bold, 12'".format(title))
write2pipe(plot.stdin, "set xl 'Seconds' font 'inconsolata, 10'")
write2pipe(plot.stdin, "set yl 'Watts' font 'inconsolata, 10'")

#Scale
write2pipe(plot.stdin, "set yrange [0.0:5.0]")

# Open data from tarfiles
tar = tarfile.open(os.path.join(WEBDAV_FOLDER, filename), "r:gz")
data = csv.DictReader(tar.extractfile("data.csv").read().decode('utf-8').split('\n'))
timing = list()
for line in tar.extractfile("timing").read().decode('utf-8').split('\n'):
	if line:
		timing.append(line)
time_initial = float(timing.pop(0))
# Construct plot string
plot_string = "plot "
for num in range(0,len(timing) + 1):
	plot_string = plot_string + "'-' w p lt {0} ps 0.25, ".format(num + 1)
write2pipe(plot.stdin, plot_string)
# Plot Points

for time_line in timing:
	time_dif = float(time_line) - time_initial
	for data_line in data:
		if(float(data_line['time']) > time_dif):
			## Next partition
			print("Time_Dif: {0}".format(time_dif))
			write2pipe(plot.stdin, "e")	# End current partition
			write2pipe(plot.stdin, "{0} {1}".format(data_line['time'], data_line['W']))
			break
		else:
			write2pipe(plot.stdin, "{0} {1}".format(data_line['time'], data_line['W']))
# Print rest of data
for data_line in data:
	write2pipe(plot.stdin, "{0} {1}".format(data_line['time'], data_line['W']))
write2pipe(plot.stdin, "e")	# End partition

tar.close()

# Finish
write2pipe(plot.stdin, "quit")
plot.stdin.close()
plot.wait()

# Open generated file
if("png" in arguments):
	print("Content-type: image/png")
else:
	print("Content-type: image/svg+xml")
print("Cache-Control: max-age=3600\n")
sys.stdout.flush()
tfile = open(tempfilename, "rb")
sys.stdout.buffer.write(tfile.read())
tfile.close()
os.remove(tempfilename)
