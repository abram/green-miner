#!/usr/bin/python3
import os, re

website_directory = "web"
output_directory = "output"
template_directory = "templates"

# Make output directory (if non-existant)
if not os.path.exists(output_directory):
    os.makedirs(output_directory)

# Get list of all pages
pages = []
for (root, directory, listing) in os.walk(website_directory):
	for file in listing:
		# Chop off the website_directory directory
		pages.append({'dir': root[len(website_directory) + 1:], 'file': file})

re_template = re.compile(r"\{\{(.+?)\}\}")

# Replace all pages
for page in pages:
	print("=== " + os.path.join(page['dir'], page['file']) + " ===")

	# Make directory structure in output folder
	out_dest = os.path.join(output_directory, page['dir'])
	if not os.path.exists(out_dest):
		os.makedirs(out_dest)

	# Open output file for writing
	output = open(os.path.join(out_dest, page['file']), "w")

	# Open file to insert templates
	with open(os.path.join(website_directory, page['dir'], page['file']), 'r') as f:
		for line in f:
			# Look for template include
			result = re_template.search(line)
			if result is not None:
				# Print template file in place
				try:
					with open(os.path.join(template_directory, result.group(1)), 'r') as t:
						for line2 in t:
							output.write(line2.strip())
				except:
					print(os.path.join(template_directory, result.group(1)) + " does not exist, nothing included.")
			else:
				# Print line as normal
				output.write(line.strip())

	output.close()
