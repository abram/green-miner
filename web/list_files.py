#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import cgi, datetime, os, time

arguments = cgi.FieldStorage()

import cgitb; cgitb.enable()

# Print the content type
print("Content-type: text/plain\n")

# Folder locations
data_folder = 'uploads/data'
apk_folder = 'uploads/apks'
tarballs_folder = 'uploads/tarballs'

# Map folders to names
folder = {
	'apk': apk_folder,
	'data': data_folder,
	'tarballs': tarballs_folder
}

# Get the list type
list_type = arguments.getvalue('list', '').strip()

# Extract the selected folder
selected_folder = folder.get(list_type, data_folder)

# Print the files out
folder = os.path.join(os.path.join(os.getcwd(), selected_folder), arguments.getvalue('tag', ''))

for root, dirnames, files in os.walk(folder, topdown=True):
	for f in sorted(files):
		if (f.startswith('.')):
			continue
		print(f)
	break
