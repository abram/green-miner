#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import cgi, os, subprocess, sys, tempfile
import statistics
import cgitb
cgitb.enable()

arguments = cgi.FieldStorage()

RSCRIPT = os.path.expanduser("Rscript")
WEBDAV_PATH = 'uploads/data'


web_dir = os.getcwd()
with tempfile.TemporaryDirectory() as tmpdir:
    stats = statistics.calculate_statistics([arguments.getvalue('file')],
                statistics.Results())

    for name, stat in stats.stats.items():
        statistics.write_csv(stat, os.path.join(tmpdir, name))

    os.chdir(tmpdir)

    args = [RSCRIPT, os.path.join(web_dir, "stacked.R")]
    subprocess.check_call(args, stderr=subprocess.DEVNULL,
        stdout=subprocess.DEVNULL)

    sys.stdout.buffer.write("content-type: image/svg+xml\n\n".encode("UTF-8"))

    with open("stacked.svg", "rb") as svg:
        while True:
            chunk = svg.read(2048)
            if chunk:
                sys.stdout.buffer.write(chunk)
            else:
                break
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
