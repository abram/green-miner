#!/usr/bin/env python3
#
# Copyright (c) 2014 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# keep tabs on what clients are doing what

import cgi, json, os, sys, time
from errors import DB

STALE_TIME = 60 * 8 # 8 minutes = probably dead


# status is a key in the db structured like so:
#
# db[status] = { <device_id>: {
#                    'status': <status_name>,
#                    'when': <when this started>,
#                    'data': { .... }
#                 },
#                }

def update_status(db):
    statuses = db.get('status') or {}

    status = json.loads(sys.stdin.read()) # cgi is weird
    status['when'] = int(time.time())

    device = cgi.FieldStorage().getvalue('device')
    statuses[device] = status

    # weird hack thing to make sure that if something goes wrong,
    # we don't end up with confusing and useless statuses like
    # null has crashed...
    for key in [None, 'null']:
        if key in statuses:
            del statuses[key]

    db.put('status', statuses)

# we respond with
# { <device_id>: {'elapsed': <seconds in this event>,
#                 'status': <status_name>,
#                 'stale': true|false,
#                 'data': {...}}}
def get_statuses(db):
    statuses = db.get('status') or {}

    now = int(time.time())
    for device, status in statuses.items():
        status['elapsed'] = now - status['when']
        del status['when']

        # try to notice when something is dead
        max_duration = status['data'].get('duration', 0) + STALE_TIME
        status['stale'] = (status['elapsed'] > max_duration)
    
    print("Content-type: application/json")
    print()
    print()
    print(json.dumps(statuses, sort_keys=True, indent=2))

if __name__ == '__main__':
    import cgitb; cgitb.enable()

    if os.environ['REQUEST_METHOD'] == 'POST':
        with DB.open_db(3, 'c') as db:
            update_status(db)
            get_statuses(db)
    elif os.environ['REQUEST_METHOD'] == 'GET':
        with DB.open_db(3, 'r') as db:
            get_statuses(db)
