#!/usr/bin/env python3
#
# Copyright (c) 2014 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# simple service for reporting errors to the server. It does not depend
# on libgreenminer so that it is hopefully less likely to break things,
# and can be used to report errors IN libgreenminer

import cgi, datetime, dbm.gnu, json, os, sys, time

import cgitb; cgitb.enable()


# handles POST by adding to the error queue,
# handles GET by dumping the error queue

DB_FILE = 'status.gdbm'

class DB(object):
    def __init__(self, db):
        self.db = db

    @staticmethod
    def open_db(timeout, mode='r'):
        """try opening the DB, for up to timeout seconds.
        if the db is currently locked for writing, we loop without waiting
        until it is opened or we time out."""
        started_at = time.time()

        while time.time() < timeout + started_at:
            try:
                return DB(dbm.gnu.open(DB_FILE, mode))
            except dbm.gnu.error as e:
                if e.errno == 11: # resource temporarily unavailable
                    continue
                raise
        raise TimeoutError('failed to open db')

    def get(self, key):
        value = self.db.get(key)
        if value is None:
            return value

        return json.loads(value.decode('utf-8'))

    def put(self, key, value):
        self.db[key] = json.dumps(value).encode('utf-8')

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.db.__exit__(self, *args)

def add_error(db):
    errors = db.get('errors') or []

    error = json.loads(sys.stdin.read()) # cgi is weird
    error['when'] = str(datetime.datetime.now())
    errors.append(error)

    errors = errors[-20:] # just keep the 20 most recent

    db.put('errors', errors)

def list_errors(db):
    errors = db.get('errors') or []
    print("Content-type: application/json")
    print()
    print()
    print(json.dumps(errors, sort_keys=True, indent=2))


if __name__ == '__main__':
    if os.environ['REQUEST_METHOD'] == 'POST':
        with DB.open_db(3, 'c') as db:
            add_error(db)
            list_errors(db)
    elif os.environ['REQUEST_METHOD'] == 'GET':
        with DB.open_db(3, 'r') as db:
            list_errors(db)
