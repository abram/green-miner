#!/usr/bin/env python3
import argparse, csv, io, json, os, re, requests, sys, tarfile
from itertools import chain
from operator import contains, itemgetter

if __name__ == '__main__':
	config = json.loads(open(os.path.join(os.path.dirname(__file__), '..', 'config.json')).read())

	ARCHIVES_URL = config['listing_url']
	DATA_URL = config['data_url']

ARCHIVES_FOLDER = '/var/uploads/data'

class WebArchiveProvider(object):
	def get_archive_list(self):
		return (a for a in requests.get(ARCHIVES_URL, verify=False).text.split("\n") \
					if len(a) > 0)

	def get_archive(self, name):
		return io.BytesIO(requests.get(os.path.join(DATA_URL, name),
			verify=False).content)

class LocalArchiveProvider(object):
	def get_archive_list(self):
		for root, dirnames, files in os.walk(ARCHIVES_FOLDER, topdown=True):
			for f in files:
				if (f.startswith('.')):
					continue
				yield f
			break

	def get_archive(self, name):
		return open(os.path.join(ARCHIVES_FOLDER, name), 'rb')


class Universe(object):
	def __contains__(self, x):
		return True

def make_set(data):
	if type(data) is str:
		return set([data])
	if data is None:
		return Universe()
	return set(data)

def archive_matches(archive, results, batch, test, app, version, device):
	# name format: {app}.{version}.{test}.{device}.{batch}.{time}.tar.gz
	# remove file extension and date
	name_parts = archive.split('.')[:-3]
	try:
		arch_app, arch_version, arch_test, arch_device, arch_batch = name_parts
	except ValueError:
		results.add_error("Incorrect archive name format", archive)
		return False

	matchers = [(batch, arch_batch), (test, arch_test), (app, arch_app),
					(version, arch_version), (device, arch_device)]
	return all(y in x for (x, y) in matchers)

class CSVAggregator(object):
	"""Class which aggregates csv results, keeping track of run numbers
		at the same time."""
	def __init__(self, headers):
		self.headers = headers
		self.rows = [["app", "version", "run", "test", "start", "device"] \
						+ headers]
		self.run_number = 0

	def process(self, app, version, test, start_time, device, rows):
		reader = csv.DictReader(rows)
		for row in reader:
			run_data = [app, version, self.run_number, test, start_time, device]
			self.rows.append(run_data + [row[col] for col in self.headers])
		self.run_number += 1

class PartitionInfoAggregator(object):
	def __init__(self):
		self.headers = ['name', 'weight', 'test']
		self.rows = [self.headers]
		self.tests = set()

	def process(self, app, version, test, start_time, device, rows):
		if test in self.tests:
			return

		self.tests.add(test)

		info_path = os.path.join('/var/green-star/tests', test, 'partition_info.csv')
		with open(info_path) as csv_file:
			reader = iter(csv.reader(csv_file))
			headers = next(reader)
			weight = headers.index('weight')
			name = headers.index('name')

			for row in reader:
				self.rows.append([row[name], row[weight], test])

class Results(object):
	def __init__(self):
		self.stats = dict()
		self.errors = dict()
		self.processed = 0

	def add_stats(self, name, stats):
		self.stats[name] = stats

	def add_error(self, msg, value):
		if msg not in self.errors:
			self.errors[msg] = []
		self.errors[msg].append(value)

def get_headers(archive, files):
	"""gets the csv headers for a list of files in an archive"""
	with archive:
		tarball = tarfile.open(mode='r:gz', fileobj=archive)

		def get_headers_for_file(name):
			return next(iter(tarball.extractfile(name))) \
				.decode('UTF-8').strip().split(',')
		headers = [get_headers_for_file(f) for f in files]
		return headers

def calculate_archive_stats(name, archive, stats, results):
	"""Calculates partition and totals stats for an archive,
		adding them to the aggregators provided"""
	with archive as archive:
		try:
			tarball = tarfile.open(mode='r:gz', fileobj=archive)
		except tarfile.ReadError:
			results.add_error("failed to open archive", name)
			return

		try:
			info = tarball.extractfile('info.json').read().decode('UTF-8')
			info = json.loads(info)

			app, version = info['application'], info['version']
			device, test = info['device'], info['test']
			start_time = json.loads(tarball.extractfile('before.json').read().decode('UTF-8'))['time']

			for file_name, aggregator in stats.items():
				data = tarball.extractfile(file_name) \
							.read().decode('UTF-8').split('\n')
				aggregator.process(app, version, test, start_time,
								device, data)
		except KeyError:
			results.add_error("archive skipped: files missing", name)
			return

		results.processed += 1

def get_archive_list(results, provider=None, batch=None, app=None,
		test=None, device=None, version=None):

	if (provider is None):
		provider = LocalArchiveProvider()

	batch = make_set(batch)
	app = make_set(app)
	test = make_set(test)
	device = make_set(device)
	version = make_set(version)

	def archive_matcher(archive):
		return archive_matches(archive, results, batch=batch, device=device,
			test=test, version=version, app=app)
	return filter(archive_matcher, provider.get_archive_list())


def calculate_statistics(archives, results, provider=None):
	"""Calculates totals and partition stats for all archives
		matching the batch/app/version/test/device values given"""
	if (provider is None):
		provider = LocalArchiveProvider()

	archives = iter(archives)
	try:
		first = next(archives)
	except StopIteration:
		results.processed = 0
		return results

	partition_headers, totals_headers = get_headers(provider.get_archive(first),
			['partitions.csv', 'totals.csv'])
	archives = chain([first], archives)

	partition_stats = CSVAggregator(partition_headers)
	totals_stats = CSVAggregator(totals_headers)
	partition_info = PartitionInfoAggregator()

	aggregators = {
		'partitions.csv': partition_stats,
		'totals.csv': totals_stats,
		'info.json': partition_info,
	}

	for f in archives:
		calculate_archive_stats(f, provider.get_archive(f),
				aggregators, results)

	def app_version_key(app_index, version_index):
		# split into alternating groups of digits and non-digits
		version_split = re.compile('(\d+)')

		def sorter(row):
			""" implements natural sorting """
			app = row[app_index]
			version = version_split.split(row[version_index])
			return [app] + [int(s) if s.isdigit() else s for s in version]
		return sorter

	def sorted_rows(rows):
		rows = iter(rows)
		headers = next(rows)
		app_index = headers.index("app")
		version_index = headers.index("version")
		return chain([headers], sorted(rows,
				key=app_version_key(app_index, version_index)))

	partition_stats = sorted_rows(partition_stats.rows)
	results.add_stats("partitions.csv", partition_stats)

	totals_stats = sorted_rows(totals_stats.rows)
	results.add_stats("totals.csv", totals_stats)

	results.add_stats("partition_info.csv", partition_info.rows)

	return results

def write_csv(stats, filename):
	with open(filename, 'w') as f:
		for row in stats:
			f.write(",".join(map(str, row)) + "\n")

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Collect a bunch of data')
	parser.add_argument('--batch', help='limit to batchs', nargs='*')
	parser.add_argument('--app', help='limit to app', nargs='*')
	parser.add_argument('--version', help='limit to versions', nargs='*')
	parser.add_argument('--test', help='test to aggregate')
	parser.add_argument('--device', help='limit to devices', nargs='*')
	parser.add_argument('--archive', help='a single archive to calculate with')
	parser.add_argument('--web', help='get archives from the web',
			dest='provider', action='store_const', const=WebArchiveProvider(),
			default=LocalArchiveProvider())
	args = parser.parse_args()

	if args.archive and (args.batch or args.commit or args.test or args.device):
		print("Cannot specify --archive and --(batch|commit|test|device)")
		exit(1)

	results = Results()
	if args.archive:
		test_stats = calculate_statistics([args.archive], results, args.provider)
	else:
		archives = get_archive_list(results, args.provider,
			batch=args.batch, app=args.app, version=args.version,
			test=args.test, device=args.device)
		test_stats = calculate_statistics(archives, results, args.provider)

	for name, stats in test_stats.stats.items():
		write_csv(stats, name)

	print("Results aggregated from {} archives".format(test_stats.processed))
	for error, values in test_stats.errors.items():
		print('error: "{0}" encountered with'.format(error))
		for v in values:
			print('\t' + str(v))
	if not test_stats.errors:
		print("no errors encountered")
