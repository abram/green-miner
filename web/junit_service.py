#!/usr/bin/env python3
#
# Copyright (c) 2014 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import cgi, json, os, requests, sys, time, uuid
import statistics

import cgitb
cgitb.enable()

arguments = cgi.FieldStorage()

GIT_REPO = os.path.expanduser("/var/green-star")
QUEUE_BASE_URL = "https://pizza.cs.ualberta.ca/gm/queue.pl"
QUEUE_URL = "https://pizza.cs.ualberta.ca/gm/queue.pl?add_tests"

json_data = {}

def show_progress():
    id = arguments.getfirst('id')
    sys.path.append(GIT_REPO)
    from libgreenminer import Queue
    queue = Queue('android')
    queue.pull()

    runs = queue.get_app_ver_total_runs('junit', id)
    json_data['id'] = id
    json_data['runs_remaining'] = runs


def schedule_tests():
    import upload_apk

    # now we have two files, the suite, and the apk, that need to
    # be saved
    assert 'app' in arguments
    assert 'suite' in arguments

    #uid = str(uuid.uuid4())
    uid = arguments['app'].filename
    json_data['id'] = uid

    

    # add the files
    upload_apk.handle_upload(arguments['app'].file,
        upload_apk.make_filename('junit', uid))
    upload_apk.handle_upload(arguments['suite'].file,
        upload_apk.make_filename('junit', uid + '_suite'))

    batch = arguments.getfirst('batch', uid)


    # now schedule the test
    response = requests.post(QUEUE_URL, data={
        'app': 'junit',
        'versions': uid,
        'how': 'suggest',
        'test': 'junit',
        'repetitions': 5,
        'batch_name': batch
    })
    assert response.ok
    json_data['repetitions'] = 5



if os.environ['REQUEST_METHOD'] == 'POST':
    schedule_tests()
elif os.environ['REQUEST_METHOD'] == 'GET':
    show_progress()

print("Content-type: text/json\n\n")
print(json.dumps(json_data))
