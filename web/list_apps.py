#!/usr/bin/env python3
#
# Copyright (c) 2014 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import cgi, json, os, pystache

from get_tests import get_tests_per_app

apk_folder = 'uploads/apks'


def list_apks_per_app():
    app_apks = {}
    for root, dirnames, files in os.walk(apk_folder, topdown=True):
        apks = [f for f in files if os.path.splitext(f)[1] == '.apk']
        apks = list(sorted(apks))
        if apks:
            app_apks[os.path.split(root)[-1]] = apks
    return app_apks

tests_per_app = get_tests_per_app()
apks_per_app = list_apks_per_app()

app_names = sorted(set(tests_per_app.keys()) | set(apks_per_app.keys()))

data = {
    'apps': json.dumps([{
        'name': app
        ,'tests': tests_per_app.get(app, [])
        ,'apks': apks_per_app.get(app, [])
    } for app in app_names])
}

print(pystache.render("""Content-type: text/html


<!doctype html>
<html><head>
    <title>available greenminer apps and tests</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="resources/bootstrap.min.css"/>
    <link rel="stylesheet" href="resources/style.css"/>
    <link href='https://fonts.googleapis.com/css?family=Inconsolata' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    </head><body ng-app>
    <div class="navbar" style="margin-bottom: 0">
        <div class="navbar-inner">
            <a class="brand" href="#">GreenMiner</a>
            <ul class="nav">
                <li><a href="index.py">Test Results</a></li>
                <li><a href="add_tests.html">Add Tests</a></li>
                <li class="active"><a href="#">Browse Tests and APKs</a></li>
                <li><a href="queue.html">Manage Queue</a></li>
                <li><a href="status.html">Status</a></li>
                <li><a href="graphs.py">Graphing</a></li>
            </ul>
        </div>
    </div>
    <div class="header">
        <h1>Greenminer Tests and APKs</h1>
    </div>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.10/angular.min.js"></script>
        <script type="text/javascript">
            function ListCtrl($scope) {
                $scope.apps = {{{apps}}};

                $scope.toggleExpanded = function(app) {
                    app.expanded = !app.expanded;
                };
            }
        </script>

    <div class="center-text">click on an app title below to see the available tests/apks</div>
    <ul ng-controller="ListCtrl" class="center content-main app-list">
        <li ng-repeat="app in apps">
            <div class="title"><a ng-click="toggleExpanded(app)" ng-bind="app.name">            </a></div>
            <div class="body" ng-show="app.expanded">
                <div class="left"><span>tests</span>
                    <ul><li ng-repeat="test in app.tests"
                            ng-bind="test"></li></ul>
                </div>
                <div class="right"><span>apks</span>
                    <ul><li ng-repeat="apk in app.apks"
                            ng-bind="apk"></li></ul>
                </div>
            </div>
        </li>
    </ul>
</body></html>""", data))
