#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import cgi, json, os
import cgitb
cgitb.enable()

arguments = cgi.FieldStorage()


if '..' in arguments.getvalue('folder'):
	exit(0)

def is_graph(path):
	return (not path.startswith('.')) and  path.endswith('.pdf')

def process_is_alive(graph_folder):
	with open(os.path.join(graph_folder, '.Rscript.pid')) as pidfile:
		pid = int(pidfile.read().strip())

		try:
			os.kill(pid, 0)
			return True
		except ProcessLookupError:
			return False


folder = os.path.join('graph', arguments.getvalue('folder'))

json_data = {}

try:
	json_data['alive'] = process_is_alive(folder)
except:
	json_data['alive'] = None

for base, folders, files in os.walk(folder):
	json_data['graphs'] = [{'name': f, 'src': os.path.join(folder, f)} \
							for f in sorted(filter(is_graph, files))]
	break

print("content-type: text/json\n")
print(json.dumps(json_data))
