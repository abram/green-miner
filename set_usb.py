#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import libgreenminer, sys

usage = "Usage: ./set_usb {device id} {on|off}"

command = None
try:
	sys.argv[2]
	sys.argv[1]
except:
	print("YOU FORGOT AN ARGUMENT.")
	print(usage)
	sys.exit(1)
try:
	if(sys.argv[3]):
		print("THERE AREN'T THAT MANY ARGUMENTS.")
		print(usage)
		sys.exit(1)
except:
	pass

builder = libgreenminer.DeviceBuilder(sys.argv[1])
arduino = builder.build_arduino()

if(sys.argv[2] == "on"):
	arduino.set_usb_on()
elif(sys.argv[2] == "off"):
	arduino.set_usb_off()
else:
	print("I DON'T KNOW WHAT TO DO WITH '" + sys.argv[2]  + "'.")
	print(usage)
	sys.exit(1)

print("Set USB", sys.argv[2], "for device", sys.argv[1] + ".")
