#!/usr/bin/env python3
#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import argparse, io, json, os, platform, subprocess, tarfile, time

config = json.loads(open('config.json').read())
SERVER = config['server']
USERNAME = config['scp']['username']
DESTINATION = config['scp']['data_folder']

def getPathInfo(path):
	"""
	Splits the path and cleans the split path of all periods.
	Replaces periods with underscores.

	NOTE: Splits off the path at the end of the string!
	"""
	path, name = os.path.split(path)
	return (path, name.replace('.', '_'))

class AndroidTestRun:
	def __init__(self, path):
		"""
		Takes a path with the following format:
			{Device ID}/{Batch Name}/{Test Name}/{App Name}/{Version}/{Run Number}
		"""
		self.path = path

		self.data = dict()
		# Get Test Info
		path, self.data['run'] = getPathInfo(path)
		path, self.data['version'] = getPathInfo(path)
		path, self.data['application'] = getPathInfo(path)
		path, self.data['test'] = getPathInfo(path)
		path, self.data['batch'] = getPathInfo(path)
		path, self.data['device'] = getPathInfo(path)

		# Get Device Info
		with open('devices.json', 'r') as f:
			self.data['config'] = json.load(f)[self.data['device']]

		# Get Test Finish Time
		with open(self.path + "_after.json", 'r') as f:
			after_data = json.load(f)
			# ISO 8601 Time
			self.time = time.strftime("%Y%m%dT%H%MZ", time.gmtime(after_data['time']))

	def getJSONData(self):
		return self.data

	def getName(self):
		return "{0}.{1}.{2}.{3}.{4}.{5}".format(self.data['application'], self.data['version'], self.data['test'], self.data['device'], self.data['batch'], self.time)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Upload Test Data to Server')
	parser.add_argument('--path', help="Path to Test Run")
	parser.add_argument('--list', dest='list', help='A File Containing a List of Paths to Upload')
	parser.add_argument('--noupload', help="Suppress Upload", action='store_true')
	args = parser.parse_args()

	runs = list()
	unsuccessful = list()

	# Add path
	if(args.path):
		runs.append(args.path)
	# Add all the paths in the list file
	if(args.list):
		try:
			with open(args.list, 'r') as f:
				for line in f.readlines():
					runs.append(line.strip())


		except:
			# New list
			pass

	# Make .tar.gz & Upload
	for run in runs:
		test = AndroidTestRun(run)

		run_path, run_number = os.path.split(test.path)
		tar_path = os.path.join(run_path, test.getName() + ".tar.gz")

		if(not os.path.exists(tar_path)):
			# Make the tar if it doesn't exist
			tar = tarfile.TarFile.open(tar_path, 'w:gz')
			# Add data to tar file
			for f in os.listdir(run_path):
				if(f == run_number):
					filename = "data.csv"
					tar.add(os.path.join(run_path, f), filename)
					# Wattlog file has the same name as the run number
				elif(f.startswith(run_number + '_')):
					# Other files have the run number with an underscore appended
					filename = f[len(run_number + '_'):]
					tar.add(os.path.join(run_path, f), filename)
				

			# Add info to tar file
			data = test.getJSONData()
			# add our own info
			data.update({
				'hostname': platform.node(),
			})
			data = json.dumps(data).encode('utf8')
			info = io.BytesIO(data)
			tar_info = tarfile.TarInfo('info.json')
			tar_info.mtime = time.time()
			tar_info.size = len(data)
			tar.addfile(tar_info, info)
			tar.close()

		if(not args.noupload):
			try:
				# SCP tar.gz to server
				subprocess.check_output(['scp', tar_path, USERNAME + '@' + SERVER + ':' + DESTINATION])
			except Exception as e:
				# Upload failed
				print("Error: ", str(e))
				unsuccessful.append(run)

	if(args.list):
		if(unsuccessful):
			listfile = open(args.list, 'w')
			for run in unsuccessful:
				listfile.write(run + '\n')
			listfile.close()
			# Write back unsucessful uploads to list
		else:
			# Clear list
			open(args.list, 'w').write('')
