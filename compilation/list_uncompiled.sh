# lists all of the commits that are available in ~/src/mozilla-central,
# but are not available as apks on the webdav server
#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

WD=`pwd`

available_list=`mktemp`
compiled_list=`mktemp`

# get a list of available commits
cd ~/src/mozilla-central
hg log --template '{node}\n' > $available_list
cd $WD

# get a list of compiled commits
curl --user `cat ~/auth_webdav` 'http://www.fennec.softwareprocess.es/webdav/images/' | egrep -o 'fennec.*\.apk' | sed 's/fennec_[0-9]\+_\([a-z0-9]\+\)\.apk/\1/' > $compiled_list


./listdiff.py $available_list $compiled_list
