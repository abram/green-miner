#! /usr/bin/perl
#
# Example usage:
#   ./comp_conductor.pl 6 todo.list [niceness]
#
# Copyright (c) 2013 Jed Barlow, Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

my ($num_processes, $list_file, $niceness) = @ARGV;

# let's be nice
print "using default niceness of 19\n" unless defined $niceness;
setpriority(0, 0, defined $niceness ? $niceness : 19);


#{{{ Make a list of available log file names
my @log_files;
for (my $lf = "", my $n = 0; @log_files < $num_processes; $n++) {
    push @log_files, $lf unless (-e ($lf = "./compile_log_$n"));
}
#}}} Make a list of available log file names

#{{{ Read list of commits
open FILE, "<", $list_file or die "*** Error: Can't open $list_file for reading.", $/;
my @commits = (<FILE>);
chomp @commits;
close FILE;
#}}} Read list of commits

#{{{ Divide up commits into queues
my @queues;
my $i = 0;
for my $commit (@commits) {
    push @{$queues[$i]}, $commit;
    $i = ($i + 1) % $num_processes;
}
#}}} Divide up commits into queues

#{{{ Launch child processes to compile each queue
my @pids;

for ($i = 0; $i < $num_processes; $i++) {
    my $pid = fork;
    if(not defined $pid) {
        print "*** Error: Can't start a child process.", $/;
    }
    elsif ($pid == 0) {
        print "process $i, num queues ", scalar (@queues), "\n";
        for $commit (@{$queues[$i]}) {
            system("./compile_commit.sh $commit $log_files[$i]");
        }
        exit;
    }
    else {
        push @pids, $pid;
    }
}
#}}} Launch a child process to compile each queue


#{{{ Wait for children processes to finish
for $pid (@pids) {
    waitpid($pid, 0);
}
#}}} Wait for children processes to finish
