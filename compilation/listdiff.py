#!/usr/bin/env python
#
# usage: ./listdiff.py this.list that.list
#
# prints all commits that are in this.list, but not that.list. I.e.
# the set difference between the two.
#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys

diff = set((h for h in open(sys.argv[1], 'r'))) - set((h for h in open(sys.argv[2], 'r')))


print '\n'.join(x.strip() for x in diff)
