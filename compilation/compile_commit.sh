#! /bin/bash
# compile_commit.sh
#
# Invoked with
#   ./compile_commit.sh COMMIT [BASE_DIR]
# this script does the following:
#   - Clones the mozilla-central repo into a new a
#     directory [BASE_DIR/]build_REVISION_COMMIT
#     - Uses a default directory for mozilla-central if $MAIN_FENNEC_REPO is
#       not set
#   - Launches compilation of the COMMIT
#     - Uses a default location for the mozconfig file if $MOZCONFIG is not set
#   - Launches apk packaging of the COMMIT
#
# Copyright (c) 2013 Jed Barlow, Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

COMMIT=$1
COMPACT_LOG=`readlink -m "$2"`
CURL_USER=`cat ~/auth_webdav`

function log {
    if [ $COMPACT_LOG ]; then
        echo "$COMMIT: " "$*" >> $COMPACT_LOG
    fi
    echo "$COMMIT: $*"
}


log "---" `date` "Starting commit compilation script ---"

if [ -z "$MAIN_FENNEC_REPO" ]; then
    MAIN_FENNEC_REPO=~/src/mozilla-central
fi
if [ -z "$MOZCONFIG" ]; then
    MOZCONFIG=$PWD/mozconfig-droid
fi
REV=`cd $MAIN_FENNEC_REPO; hg log -r $COMMIT --template '{rev}\n'`

COMMIT_DIR=build_${REV}_$COMMIT
if [ -z "$COMPILATION_HOST_DIR" ]; then
    COMPILATION_HOST_DIR=~/comp
fi
COMMIT_DIR=$COMPILATION_HOST_DIR/$COMMIT_DIR

log "Revision:  $REV"
log "Target Dir: $COMMIT_DIR"

log "Cloning $MAIN_FENNEC_REPO"
hg clone $MAIN_FENNEC_REPO $COMMIT_DIR
if [ "$?" != "0" ]; then
    log "***Error: Failed to clone the main repository"
    exit 1
fi
cd $COMMIT_DIR
log "Checking out commit"
hg checkout $REV
if [ "$?" != "0" ]; then
    log "***Error: Failed to checkout the commit"
    exit 1
fi

export MOZCONFIG
#export CFLAGS="-Wno-error=unused-result"
#export CXXFLAGS="-Wno-error=unused-result"
log "Compiling"
USER='greenminer' make -f client.mk build > ${COMMIT_DIR}/compile_log 2>&1
if [ "$?" != "0" ]; then
    log "***Error: Failed to compile"
    exit 1
fi

# For older commits, might have to use 'package' instead of 'fast-package'
USER='greenminer' make -f client.mk fast-package > ${COMMIT_DIR}/package_log 2>&1
if [ "$?" != "0" ]; then
    USER='greenminer' make -f client.mk package > ${COMMIT_DIR}/package_log 2>&1
    if [ "$?" != "0" ]; then
        log "***Error: Failed to package the .apk"
        exit 1
    fi
fi

# Upload a copy of the .apk to fennec.softwareprocess.es
log "Uploading package to fennec.softwareprocess.es"
FENNEC_APK=`ls ${COMMIT_DIR}/objdir-droid/dist/*fennec*.apk`
curl --user $CURL_USER -f -T $FENNEC_APK http://www.fennec.softwareprocess.es/webdav/images/fennec_${REV}_${COMMIT}.apk
if [ "$?" != "0" ]; then
    log "***Error: Failed to upload the .apk"
    exit 1
fi

# Cleanup
log "Cleaning up (removing target dir)"
rm -R $COMMIT_DIR

log "Finished commit compilation"
exit 0

# Don't install the apk here, that is handled when running the
# tests.
