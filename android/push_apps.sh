#!/usr/bin/env sh
# Installs some special apps to the phone to make our lives easier.

# Put files to sdcard
adb push microtime/obj/local/armeabi/microtime /sdcard/
adb push microsleep/obj/local/armeabi/microsleep /sdcard/
adb push tapnswipe/obj/local/armeabi/tapnswipe /sdcard/
adb push sqlite3 /sdcard/
adb push busybox /sdcard/

# Mount /system/
adb shell su -c "mount -o remount,rw /system"

# Move files
adb shell su -c "cp /sdcard/microtime /system/xbin"
adb shell rm /sdcard/microtime
adb shell su -c "cp /sdcard/microsleep /system/xbin"
adb shell rm /sdcard/microsleep
adb shell su -c "cp /sdcard/tapnswipe /system/xbin"
adb shell rm /sdcard/tapnswipe
adb shell su -c "cp /sdcard/sqlite3 /system/xbin"
adb shell rm /sdcard/sqlite3
#adb shell su -c "cp /sdcard/busybox /system/xbin"
adb shell rm /sdcard/busybox

# Fix permissions
adb shell su -c "chmod 755 /system/xbin/microtime"
adb shell su -c "chmod 755 /system/xbin/microsleep"
adb shell su -c "chmod 755 /system/xbin/tapnswipe"
adb shell su -c "chmod 755 /system/xbin/sqlite3"
adb shell su -c "chmod 755 /system/xbin/busybox"

#Reboot
#adb reboot
