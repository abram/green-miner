#!/usr/bin/env python
#
# Averages a number of power consumption readings from the arduino.
#
# How to Use:
#	./read_average device number_readings
#
# Sample Usage:
#	./read_average /dev/ttyACM0 10000
#
# Copyright (c) 2013 Alex Wilson, Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import serial, sys, re, time

number = int(sys.argv[2])
i = 0

# Setup Serial
ser = serial.Serial()
ser.baudrate = 19200
ser.port = sys.argv[1]
ser.timeout = None

# Match format for reading from arduino
regex = re.compile("^[A-Z]\t[\+#]\t(-?[0-9\.]{4,7})\t([0-9\.]{4})")
onethousand = [0] * number

# Start Reading Serial
ser.open()
time.sleep(0.5)
print(ser.read(ser.inWaiting()))

while True:
	# Get reading from device
	line = ser.readline().strip()
	m = regex.match(line)

	if m is not None:
	# Reading is valid
		print(line)
		onethousand[i] = float(m.group(1))
		i += 1

	if i == number:
	# Average all the readings
		print("Average of {0} readings: {1}.".format(str(number), str(sum(onethousand)/number)))

	# Close serial
		ser.close()
		exit()

ser.close()
