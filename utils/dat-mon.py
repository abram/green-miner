#!/usr/bin/env python2.7

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import shutil
import subprocess
import os

# Cron job to run on pi's: 0 */4 * * * /usr/bin/python /home/pi/green-star/utils/dat-mon.py

# print size of the dat folder. If it is larger than 1000M delete it. The default output is in kilobytes.
size_KB = subprocess.Popen("du -s /home/pi/dat/ | cut -f 1",
                        shell=True,
                        stdout=subprocess.PIPE).stdout.read()
print(size_KB)
size_KB = size_KB.strip("\r\n")
size_KB = int(size_KB)
size_MB = size_KB / 2 ** 10


if (size_MB > 1000):
    print("Found large dat folder. Deleting content.")

    # If you want a backup, here is the code. A bit of time to complete.
    #print("Creating backup.")
    #subprocess.Popen("tar -zcf /home/pi/dat.back.tgz /home/pi/dat/")
    pathname = os.path.abspath(os.path.join("/home/pi/", "dat/"))

    shutil.rmtree(pathname)
