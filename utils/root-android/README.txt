These files are for being able to root an emulator to install tapnswipe things
on it. Below are some instructions on how to root an emulator. The script
is Francesco's! Also it will only work on an ARM galaxy nexus emulator.


Rooting the emulator
====================
You'll have to install the superuser on the emulator. To do this, unzip the 
root-android file somewhere. cd into it, and edit Francesco's root.sh script. 
Just change the two lines at the top defining the path to wherever you unzipped
the root-android dir, and the path leading to the green-star android folder.  
Eg.  
    ROOT_ANDROID_PATH="/home/sgil/summer2014/green-star/android/root-android"  
    GREENSTAR_ANDROID_PATH="/home/sgil/summer2014/green-star/android"  

Make sure you give this script executable permission by running  
`chmod +x root.sh`   
  
Also, cd to the green-star android folder. Give executable permission to the
push_apps.sh (`chmod +x push_apps.sh`). Also, open up push_app.sh in an
editor and comment out the last line, which is `adb reboot`. For some reason
this causes the emulator to crash.  

Go back to where the root.sh script is, have your emulator running, and   
run the script. That should work...

IMPORTANT:
If root.sh doesn't work and it complains that there are locations on the phone that are readonly, you'll have to first follow these steps:

In the terminal type

1. `adb shell`
2. `su`
3. `mount -o rw,remount rootfs /`
4. `chmod 777 /sdcard`
5. `exit`

Now running root.sh should work.
