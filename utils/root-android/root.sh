#!/bin/bash
    ROOT_ANDROID_PATH="/home/shaiful/gitlab_repos/green-star/utils/root-android"
    GREENSTAR_ANDROID_PATH="/home/shaiful/gitlab_repos/green-star/android"
	 
	echo "Remounting your device..."
	adb shell  mount -o rw,remount /system

	echo "Pushing binaries..."
	cd $ROOT_ANDROID_PATH/Superuser-3.0.7-efghi-signed/system/bin
	adb push su /system/xbin/
	cd $GREENSTAR_ANDROID_PATH
	adb push busybox /system/xbin/
	 
	echo "Setup rights..."
	adb shell chmod 06755 /system/xbin/su
	adb shell chmod 06755 /system/xbin/busybox
	 
	echo "Installing Superuser.apk..."
	cd $ROOT_ANDROID_PATH/Superuser-3.0.7-efghi-signed/system/app
	adb install Superuser.apk

    echo "Launching push_apps.sh"
    cd $GREENSTAR_ANDROID_PATH
    sh push_apps.sh
    
    echo "Done"
    
