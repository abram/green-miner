
in tests/
   shaiful_many_apps/
   shaiful_randomtest_energy/
   shaiful_randomtest_strace/
   shaiful_cpu_randomtest/

These test-batches run a variant of the regular green miner tests. In each of
the tests, the 'apk' version has been replaced with seperate apks. This
allows a researcher-defined batch to test multiple seperate apks at the same
time. It allows a researcher to more easily aggregate the results of separate
apk tests.