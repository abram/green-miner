convert_script.py converts between tests written using tapnswipe and tests
written for inputs and taps that work with the emulator. 

Can be run as follows:
$python convert_script.py <script.sh> <output.sh> [-r]

If -r is set, then the script will convert from emulator friendly taps and
swipes to tapnswipe format. If it isn't set, it will convert from tapnswipe
to emulator friendly.

To run the converted script on the emulator, run run_emu_script.sh, and give it
the name of the converted script as an argument.

Eg. ./run_emu_script.sh converted_script.sh


See the wiki page
Using an Emulator (https://pizza.cs.ualberta.ca/gitlab/green-star/wikis/using-an-emulator) and the code documentation for more information. 
