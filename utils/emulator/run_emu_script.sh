#!/bin/bash  
## Installs the test script onto phone and runs it

# Test script to be pushed onto phone
INPUT_SCRIPT=$1

if [ $# != 1 ];
then
    echo "Missing parameter: ./run_emu_script.sh <script-file>"
else
    adb push $INPUT_SCRIPT /sdcard/
    adb shell sh /sdcard/$INPUT_SCRIPT
fi
