#!/usr/bin/env python3
# Records data from the arduino for logging power consumption information
#
# How to Use:
#	   ./wattlog device_id
#
# Sample Usage:
#	   ./wattlog A > data_for_A.csv
#
# Copyright (c) 2013 Kent Rasmussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import argparse, signal, sys, time
import libgreenminer

# Print everything on exit
def exit_handler(signum, frame):
	sys.stdout.flush()
	sys.exit()
signal.signal(signal.SIGTERM, exit_handler)

# Arguments
parser = argparse.ArgumentParser(description='Records data from the arduino for logging power consumption information.')
parser.add_argument('device', metavar='device', help='Device Letter (e.g. A)')
args = parser.parse_args()

# Setup Device
builder = libgreenminer.DeviceBuilder(args.device)
arduino = builder.build_arduino()
wattlog = libgreenminer.Wattlog(arduino)

print(wattlog.return_header())

arduino.flush_buffer()
start_time = time.time()

while True:
	try:
		print(wattlog.return_line(arduino.get_reading(), time.time() - start_time).strip())
	except KeyboardInterrupt:
		exit_handler(0, 0)
		break
	except:
		# Empty or malformed reading
		continue
