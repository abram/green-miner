#!/usr/bin/env python
#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse, csv, json, time
from subprocess import Popen, PIPE

# note: we can run wattlog and pipe it into this or something, and see if it is formatting decently/correctly
parser = argparse.ArgumentParser(description='Upload test data')
parser.add_argument('deviceid', help="device id to test with")
parser.add_argument('-timeout', help="seconds to test for", type=int,
				default=20)

args = parser.parse_args()

port = json.load(open('devices.json'))[args.deviceid]['port']

print 'testing {0} seconds of wattlog output on {1}'.format(args.timeout, port)

start = time.time()
wattlog = Popen(["./wattlog.py", port], stdout=PIPE, stderr=PIPE)
reader = csv.reader(wattlog.stdout)
for row in reader:
	try:
		assert len(row) == 9, "9 columns expected"
		assert row[1] == args.deviceid, "device id should be in column 1"
		assert 0 <= float(row[2]) and float(row[2]) <= 1023,	\
			"adc1 should be in range(0, 1024)"
		assert 0 <= float(row[3]) and float(row[3]) <= 1023,	\
			"adc2 should be in range(0, 1024)"
	except AssertionError as e:
		wattlog.kill()
		wattlog.communicate()
		print "test failed with the following error:", e.message
		print row
		exit(1)

	if (time.time() - start > args.timeout):
		exit(0)

# if we get to here, wattlog closed without giving us any output
print wattlog.stderr.read()
exit(1)
