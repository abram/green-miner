#
# Copyright (c) 2013 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

DEVICE_ID=$1

IMAGE_FOLDER=~/images/

Passed() {
	Msg 'all sanity tests passed!'
	exit 0
}


Msg() {
	echo " *** MSG: $1"
}

EnterTest() {
	echo " *** TEST: $1"
}

Error() {
	echo " *** ERROR: $1"
	exit 1
}

Msg 'running sanity checks'

EnterTest 'try upload_test_data --noupload and make sure it runs'
####################################
DEST="regression_tests/data/C/no_batch/NEW_anim_svg/chrome/27.0.1453.90/chrome.27_0_1453_90.NEW_anim_svg.C.no_batch.20130704T0140Z.tar.gz"
./upload_test_data.py --path regression_tests/data/C/no_batch/NEW_anim_svg/chrome/27.0.1453.90/0 --noupload > /dev/null

if [ $? -ne 0 ]; then
	rm -f $DEST
	Error "upload_test_data.py failed"
fi

if [ -z "`file $DEST | grep 'gzip compressed data'`" ]; then
	rm -f $DEST
	Error "upload_test_data.py not a gzipped tarball!"
fi

rm -f $DEST


if [ -z $DEVICE_ID ]; then
	Msg "no device id, so skipping tests which require it"
	Passed
fi

EnterTest 'test get_device_info'
####################################
DEVICE_INFO=`./get_device_info.py $DEVICE_ID 2>/dev/null`
if [ $? -ne 0 ]; then
	Error "get_device_info.py failed"
fi

if [ `echo $DEVICE_INFO | sed 's/null/null\n/g' | grep -c 'null'` -ge 2 ]; then
	Error "too many nulls in get_device_info.py result"
fi

EnterTest 'Test wattlog output'
######################################
./regression_tests/check_wattlog_output.py $DEVICE_ID
if [ $? -ne 0 ]; then
	Error "wattlog appears not to be working correctly"
fi

Passed
