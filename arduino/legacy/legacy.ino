/*
 Reads and averages voltage across a shunt, internal arduino voltage, and
 controls a transistor via. digital signal to turn on/off power to a USB
 device.

 Copyright (c) 2013 Abram Hindle, Jed Barlow, Kent Rasmussen

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define MY_ID           'A'

#define N               64

#define STATE_ON        '+'
#define STATE_OFF       '#'

#define USB_DISCON_PIN  4
#define VOLT_PIN        A0
#define AMP_PIN         A5

char    state = STATE_OFF;
int     iterator = 0;
int     amps[N];
int     volts[N];
unsigned int ampsum = 0;
unsigned int voltsum = 0;
float   arms = 0.0;
float   vrms = 0.0;

// See http://code.google.com/p/tinkerit/wiki/SecretVoltmeter
long readVcc() {
  long result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = 1126400L / result; // Back-calculate AVcc in mV
  return result;
}

void setup() {
  // Initialize Array to 0
  for(int i = 0 ; i < N; i++) {
    amps[i] = 0;
    volts[i] = 0;
  }

  // Disconnect USB
  pinMode(USB_DISCON_PIN,   OUTPUT);
  digitalWrite(USB_DISCON_PIN,   HIGH);

  // Initialize Communication with Computer
  Serial.begin(19200);
}

void loop() {
  // Computer is Sending Info
  if(Serial.available() > 0) {
    int ser_dat = Serial.read();

    // Connect Phone
    if (ser_dat == '0') {
      digitalWrite(USB_DISCON_PIN,   LOW);
      state = STATE_ON;
    }

    // Disconnect Phone
    else if (ser_dat == '1') {
      digitalWrite(USB_DISCON_PIN,   HIGH);
      state = STATE_OFF;
    }
  }

  // Read Voltage
  voltsum = voltsum - volts[iterator];
  volts[iterator] = analogRead(VOLT_PIN);
  voltsum = voltsum + volts[iterator];

  // Read Amperage
  ampsum = ampsum - amps[iterator];
  amps[iterator] = analogRead(AMP_PIN);
  ampsum = ampsum + amps[iterator];

  iterator = iterator + 1;

  if (iterator == N) {
    iterator = 0;

    vrms = (float) voltsum / N;
    arms = (float) ampsum / N;

	// Send Averaged Readings
    Serial.print(MY_ID);
    Serial.print("\t");
    Serial.print(state);
    Serial.print("\t");
    Serial.print(vrms);
    Serial.print("\t");
    Serial.print(arms);
    Serial.print("\t");
    Serial.println(readVcc(), DEC);
  }
}
