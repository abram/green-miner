#!/usr/bin/env python3
#
# Copyright (c) 2014 Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# simple service for reporting errors to the server. It does not depend
# on libgreenminer so that it is hopefully less likely to break things,
# and can be used to report errors IN libgreenminer

import json, logging, sys
from traceback import *

import requests


def get_report_url():
    # fallback web root in case config.json is broken!
    web_root = 'https://pizza.cs.ualberta.ca/gm/'

    try:
        json.load(open('config.json'))['web_root']
    except:
        pass
    return web_root + '/errors.py'


class ErrorReporter(object):
    '''context handler that will log all errors'''

    def __init__(self, device):
        self.device = device

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.report_error(self.format_exc_info(*args))

    def format_exc_info(self, exc_type, exc_val, exc_tb):
        return '\n'.join(format_exception(exc_type, exc_val, exc_tb))

    def report_error(self, exc, message=None, run_info=None):
        report = {
            'device': self.device,
            'traceback': exc
        }
        if message is not None:
            report['message'] = message
        if run_info is not None:
            report['run_info'] = run_info

        response = requests.post(get_report_url(), json.dumps(report))

class LogHandler(logging.Handler):
    ''' Python logging handler that wraps ErrorReporter. You can include
        run_info as extra data and it will be logged.
        
        run_info=dict(test='editor')
        logging.exception('darn', extra=dict(run_info=run_info))'''

    def __init__(self, device):
        self.reporter = ErrorReporter(device)
        super().__init__(logging.ERROR)

    def emit(self, record):
        try:
            self.reporter.report_error(
                self.reporter.format_exc_info(*record.exc_info),
                record.message,
                getattr(record, 'run_info', None))
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

if __name__  == '__main__':
    import argparse, sys
    parser = argparse.ArgumentParser(description='Report errors from stdin')
    parser.add_argument('device', help="device id")

    args = parser.parse_args()

    ErrorReporter(args.device).report_error(sys.stdin.read())
