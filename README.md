see also: https://pizza.cs.ualberta.ca/gitlab/green-star/wikis/home

See the paper: http://webdocs.cs.ualberta.ca/~joshua2/greenminer.pdf

## Requirements

* Perl
    - libjson-perl
    - libberkeleydb-perl
* Python3
    - nose ((sudo) pip install nose)
        - For running unit tests
    - pystache ((sudo) pip install pystache)
        - For web interfaces
    - requests ((sudo) pip install requests)
    - python-serial
* adb
    - To connect to android devices
    - Debian: android-tools-adb
    - Raspberry Pi: http://forum.xda-developers.com/showthread.php?t=2047891
* R and ggplot2
    - For graphing

## Configuration
* Rather than installing each python library individually, you can use
	pip install -r requirements.txt
* Many scripts read a config.json file in the root. This provides a central
  location for values that will be used in many places, and may change often
(mostly URLs).
* If an app needs authentication info, this can be specified in the
  ~/tests\auth.json file. This way auth info can be kept out of git. The file
	should contain a JSON dictionary which will be passed in to the script
	template as AUTH. If you have a subdictionary named reddit, for instance,
	you can use {{#AUTH}}{{#reddit}}echo {{username}} {{password}} {{/reddit}}{{/AUTH}} to echo the username and password.
get the password).

## Arduinos
Each arduino has a 1 letter identifier (as a sticker on the device), and a path when it is connected to the computer (eg. /dev/ttyACM0).

### Scripts
* set\_usb.py {identifier} (on|off)
    - Turns the USB power on the Arduino circuitry on or off
* charge\_phone.py {identifier}
    - Attempts to charge the phone to 100% or until it reports full, but will stop if the phone stops showing differences in battery level charge over the span of 10 minutes.

## Testing
Tests live in the tests/ folder. With each subfolder being a single test. Each test however, can be customised for several apps. Each app should get its own subfolder, with a

* script.sh file
    - The shell script which will be run on the device. Before being run, it
	is templated with information about the test run. The templating language
	is the standard [Mustache](http://mustache.github.io/mustache.5.html)
	one.
* duration
    - A file specifying how long to run the test for
* test.py [optional]
	- A python file used for customising the test run. It should define a class
	named Test which subclasses libgreenminer.Test. Common uses are to override
	the before(), after(), or get_template_data() methods. See
	libgreenminer/test.py for more customisations. This file can also exist
	outside of the app folder. The resolution order for Test
	classes is: app-specific > testspecific > libgreenminer.
	

### The Online Queue
* web/queue.pl needs to be placed on a CGI server. It uses Berkeley DB to provide
    a JSON-based API for adding/getting tests.
* The web/add\_tests.py cgi script provides an html form for easily adding new
	tests. To use this form, select any of thests you wish to be run, the
	number of repetitions, and the full apk ids (whitespace
	separated) for each commit you want tested.
* Clients run the process\_queue.py script, which downloads tasks from the queue
    server, runs them, and then reports them as finished. If there are no tests
    in the queue, it will poll the server at one minute intervals for new jobs.
* You can remove items from the queue without running them with the
	remove\_from\_queue.py script. Run remove\_from\_queue.py -h for help.
* web/report.py displays a report for a single test run
* web/index.py displays a list of test runs, with sorting and searching
* web/graphs.py aggregates and graphs data from many runs
* web/make\_graphs.R uses R and ggplot2 to graph the data collected by
	graphs.py/statistics.py
* count\_queue.py summarizes run numbers/times for the queue.

### Firefox builds
* The Firefox hg repo should be cloned to ~/src/mozilla-central
    - hg clone http://hg.mozilla.org/mozilla-central/ ~/src/mozilla-central
* compile\_commit.sh does not make the build dir
* comp\_conductor.pl {num\_threads} {commits.list} [{niceness} = 19]
    - compiles Firefox apks for each revision given in the {commits.list} file, up to {num\_threads} at a time. This process and all of its children are automatically niced to the specified level (or 19 by default).
* the list\_uncompiled.sh and filter.py scripts in compilation/ can be used to simplify creating a list of commits to give to comp\_conductor.pl
* mozconfig-droid contains autoconf options for the Firefox build

### Scripts
* Upload\_test\_data.py {path to csv wattlog file}
    - Creates a .tar containing the test data, and uploads it to the server
    - Run automatically as a part of run\_test.py
* run\_test.sh {script} {duration} {commit}
    - Launches the monkey script {script} on firefox revision {commit} for {duration} seconds.
    - This script calls get\_device\_info.py (before and after the test) and \_upload\_test\_data.py.
* get\_device\_info.py {identifier}
    -  prints info about the device, thd host, time, and environment in JSON format
* mount\_apks.sh
	- attempts to make a fuse SSHFS mount of the scp.apks\_folder at images\_folder (from config.json)

## Unit/Regression Tests
* unit tests for libgreenminer are in libgreenminer/tests and can be run with 'cd libgreenminer; nosetests'
* other regression tests are in regression\_tests/. These tests can be
	run with regression\_tests/run.sh, and will include the libgreenminer
	unit tests.
