# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.wafb4e0992bd5deaf0db2eff86777d086/com.wafb4e0992bd5deaf0db2eff86777d086.MainNavigationActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 423 1063
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 159 831
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 623 488
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 815
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 552 347
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 330 336
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 281 781
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 135 881
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 126 185
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 424 618
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 457 914
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 502 1025
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 1000
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 157 809
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 550 864
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 212 443
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 573 902
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 82 632
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME