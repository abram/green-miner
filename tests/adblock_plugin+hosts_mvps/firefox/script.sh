#
# Adblock Test (None)
#	Loads a series of three pages, scrolling around a bit.
#
# Copyright (c) 2013 Jed Barlow, Kent Rasmussen, Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
am start -n org.mozilla.firefox/.App
microsleep 10000000
# Don't send data
tapnswipe /dev/input/event1 tap 553 376
# Setup Adblock Plugin (Adblock Plus)
tapnswipe /dev/input/event1 tap 400 100
microsleep 1500000
input text "http://pizza.cs.ualberta.ca/gm/tests/files/adblock_plus-2.3-an+sm+tb+fx.xpi"
tapnswipe /dev/input/event1 tap 667 119
microsleep 9000000
tapnswipe /dev/input/event1 tap 506 747
microsleep 5000000
# Block Malware
tapnswipe /dev/input/event1 swipe 590 560 600 560 100
microsleep 5000000
# Remove Social Media Buttons
tapnswipe /dev/input/event1 swipe 590 610 600 610 100
microsleep 5000000
# Disable Tracking
tapnswipe /dev/input/event1 swipe 590 670 600 670 100
microsleep 10000000
# Open Adblock Plus Settings
tapnswipe /dev/input/event1 tap 677 100
microsleep 1000000
tapnswipe /dev/input/event1 tap 475 743
microsleep 1000000
tapnswipe /dev/input/event1 tap 440 369
microsleep 5000000
tapnswipe /dev/input/event1 tap 364 292
microsleep 5000000
# Disable "Allow Some Non-Intrusive Advertising"
tapnswipe /dev/input/event1 tap 637 811
microsleep 5000000

# Close Tabs
tapnswipe /dev/input/event1 tap 574 118
microsleep 1000000
tapnswipe /dev/input/event1 tap 683 715
microsleep 1000000
tapnswipe /dev/input/event1 tap 683 481
microsleep 1000000
tapnswipe /dev/input/event1 tap 683 262
microsleep 61000000

### Google
# Enter URL
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "http://www.google.ca/search?nomo=1&q=cheap+flights"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
# Take Screenshot
{{{timing}}}
screencap -p /sdcard/screen_google.png
microsleep 1000000

### Android
# Enter URL
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "http://www.milliondollarhomepage.com/"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
# Take Screenshot
{{{timing}}}
screencap -p /sdcard/screen_million.png
microsleep 1000000

### Filestube
# Enter URL [NSFW]
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "http://www.filestube.com/query.html?q=greenminer+gm"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
# Take Screenshot
{{{timing}}}
screencap -p /sdcard/screen_filestube.png
microsleep 1000000

### Engadget
# Enter URL
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "http://www.engadget.com/2012/06/18/android-wakelock-api-bugs/"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
# Take Screenshot
{{{timing}}}
screencap -p /sdcard/screen_engadget.png
microsleep 1000000

### American Idol Wikipedia Clone
# Enter URL
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "http://pizza.cs.ualberta.ca/gm/tests/American_Idol.html"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
# Take Screenshot
{{{timing}}}
screencap -p /sdcard/screen_wikipedia.png
microsleep 1000000

### The Guardian
# Enter URL
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "http://www.theguardian.com/media/2011/oct/31/news-website-design-ads"
tapnswipe /dev/input/event1 tap 667 119
{{{timing}}}
microsleep 60000000
# Take Screenshot
{{{timing}}}
screencap -p /sdcard/screen_guardian.png
microsleep 1000000

# "Exit" Process
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
