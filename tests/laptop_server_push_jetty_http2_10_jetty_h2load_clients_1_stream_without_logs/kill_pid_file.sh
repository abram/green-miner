#!/bin/bash


LOGFILE=/tmp/kill_pid.log

echo "$(date "+%m%d%Y %T") : KIll pid. Start Checking If pid file exists" >> $LOGFILE 2>&1
if [ -e /tmp/httpclient.pid ]; then
	echo "$(date "+%m%d%Y %T") : KIll pid. If condition satisfied pid file exists" >> $LOGFILE 2>&1
    PID=$(cat /tmp/httpclient.pid)
    if  ps -p $PID > /dev/null 2>&1; then
		# the process with $PID is running. Kill it.
		kill -9 `cat /tmp/httpclient.pid`
		echo "$(date "+%m%d%Y %T") : KIll pid. Process was running killed it" >> $LOGFILE 2>&1
	fi
	echo "$(date "+%m%d%Y %T") : KIll pid. remove pid file" >> $LOGFILE 2>&1
	rm -f /tmp/httpclient.pid
fi
