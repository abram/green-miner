import libgreenminer,time,subprocess

class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		run.phone.shell('settings put system screen_off_timeout 1800000').strip()

        # Turn off Airplane mode/make sure it's off
		run.phone.shell('settings put global airplane_mode_on 0')
		run.phone.shell('am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false')
		
		run.phone.adb("push traceTools/strc_gen.sh /sdcard/strc_gen.sh")
		run.phone.shell("su -c 'cp /sdcard/strc_gen.sh /data/local/' ")
		run.phone.adb("push traceTools/strace /sdcard/")
		run.phone.shell("su -c 'cp /sdcard/strace /data/local/' ")
		run.phone.shell("su -c 'rm /sdcard/strace' ")
		run.phone.shell("su -c 'rm /sdcard/strc_gen.sh' ")
		run.phone.shell("su -c 'chmod 0777 /data/local/strc_gen.sh'")
		run.phone.shell("su -c 'chmod 0777 /data/local/strace'")
		subprocess.call(" adb shell busybox nohup su bash -c \"(sh /data/local/strc_gen.sh com.accuweather.android &) \" &", shell=True)


		
	def after(self, run):
		
		
		run.phone.shell('settings put system screen_off_timeout ' + self.timeout)

	        # Obtain screenshot
	        #run.phone.adb("pull /sdcard/accuweather.png " + run.version + ".png")
	        #run.phone.shell("rm /sdcard/accuweather.png")

		# Turn Airplane mode back on
		run.phone.shell('settings put global airplane_mode_on 1')
		run.phone.shell('am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true')

	        # Turn wifi back on
	        #run.phone.shell('svc wifi enable') - This doesn't work for some reason
		run.phone.shell('am start -a android.intent.action.MAIN -n com.android.settings/.wifi.WifiSettings')
		run.phone.shell('sleep 2')
		run.phone.shell('input tap 620 100')
		run.phone.shell('sleep 2')
		run.phone.shell('input keyevent HOME')

	def before_upload(self,run):
		#Put the files into the folder before uploading
		run.phone.adb("pull /data/local/trc.txt "+run.wattlog_file+"_strace.txt")
		run.phone.shell("su -c 'rm /data/local/trc.txt' ")
		
		
