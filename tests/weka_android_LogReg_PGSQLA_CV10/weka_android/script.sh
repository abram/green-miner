logcat > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n com.example.andrea.wekaandroidport/.MainActivity
# Choose log reg
tapnswipe /dev/input/event1 tap 50 490
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 19000000

# Train CV10
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 360
microsleep 400000000

# Validate CV10
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 600
# Split sleeps up to avoid overflow
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 850000000

# Idle time
{{{timing}}}
kill $PID
