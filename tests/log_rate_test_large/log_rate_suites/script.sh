logcat com.log.logapplication:D \*:S > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog
microsleep 10000000

# Suite 1000msg
{{{timing}}}
sh /sdcard/suites/suite_1000msg.sh
microsleep 2000000

# Suite 100msg
{{{timing}}}
sh /sdcard/suites/suite_100msg.sh
microsleep 2000000

# Suite 10msg
{{{timing}}}
sh /sdcard/suites/suite_10msg.sh
microsleep 2000000

# Suite 1msg
{{{timing}}}
sh /sdcard/suites/suite_1msg.sh
microsleep 2000000

# Suite 0.1msg
{{{timing}}}
sh /sdcard/suites/1_suite_01msg.sh
microsleep 2000000

# Suite 00.1msg
{{{timing}}}
sh /sdcard/suites/0_suite_001msg.sh
microsleep 2000000

# idle time 
kill $PID
{{{timing}}}

