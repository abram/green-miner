logcat TestLog dalvikvm:D *:S > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n org.tensorflow.demo/.CameraActivity
microsleep 2000000

{{{timing}}}
# Start evaluation
tapnswipe /dev/input/event1 tap 250 490
# Split to avoid overflow
microsleep 2000000000
microsleep 2000000000
microsleep 2000000000
microsleep 2000000000
microsleep 2000000000
microsleep 2000000000
microsleep 2000000000
microsleep 2000000000
microsleep 2000000000
microsleep 2000000000
microsleep 1800000000

# Idle time
kill $PID
{{{timing}}}
