# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.android2.calculator3/com.android2.calculator3.Calculator
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 312 170 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 615 100 100 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 171 109 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 468 717 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 100 100 615 1100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 400 762 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 100 1100 615 100 2000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 100 100 100 1100 2000
microsleep 2000000
###### tap menu ##########
tapnswipe /dev/input/event1 tap 682 1236 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 285 369 1000
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 533 105 300
microsleep 2000000
###### tap menu ##########
tapnswipe /dev/input/event1 tap 682 1236 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME