# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n org.zakky.memopad/org.zakky.memopad.PadActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 169 530 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 310 810 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 tap 615 100 2000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 413 186 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 184 476 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 615 100 100 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 179 381 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 276 231 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 100 100 615 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 402 912 300
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 100 100 100 1100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 197 887 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME