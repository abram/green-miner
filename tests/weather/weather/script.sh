# Weather (by MacroPinch) current weather test for Edmonton, Alberta, Canada

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Load App
{{{timing}}}
am start -n com.macropinch.swan/.WeatherActivity2
microsleep 40000000

# Click options 
tapnswipe /dev/input/event1 tap 650 115
microsleep 1000000

# Click settings
tapnswipe /dev/input/event1 tap 500 230
microsleep 1000000

# Turn off auto location
tapnswipe /dev/input/event1 tap 630 530
microsleep 1000000

# Return to main menu
tapnswipe /dev/input/event1 tap 30 110
microsleep 1000000

# Add new city 
tapnswipe /dev/input/event1 tap 550 115
microsleep 1000000

# Enter city: Edmonton
input text 'Edmonton,%sCanada'
microsleep 4000000

# Click Edmonton
tapnswipe /dev/input/event1 tap 360 230
microsleep 5000000

# Swipe to view the current weather
tapnswipe /dev/input/event1 swipe 650 650 100 650 3000
microsleep 5000000

# Checkout the current weather/let app idle 
{{{timing}}}
microsleep 10000000

# Take a screencap of current weather
#screencap -p /sdcard/weather.png
microsleep 10000000

# Return home
{{{timing}}}
input keyevent HOME
