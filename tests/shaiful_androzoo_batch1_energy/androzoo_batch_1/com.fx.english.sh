# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.fx.english/com.fx.english.LoadingActivity
microsleep 8000000
{{{timing}}}
###### key event ##########
input keyevent DEL
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 448 246 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 162 583 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 230 285 1000
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 541 967 1000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 81 731 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 62 439 1000
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100 300
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100 300
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 179 688 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 108 443 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 141 431 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 519 387 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME