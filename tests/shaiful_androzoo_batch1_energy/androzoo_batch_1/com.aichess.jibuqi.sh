# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.aichess.jibuqi/com.aichess.jibuqi.Pedometer
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 519 604 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 114 144 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 440 961 300
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 20 100 2000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 1000 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 223 559 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 381 405 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 693 740 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 552 369 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 513 1050 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 100 182 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 610 459 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 494 936 1000
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 336 555 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 547 239 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 1000 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 605 179 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 598 239 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 457 235 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 662 134 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 321 672 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME