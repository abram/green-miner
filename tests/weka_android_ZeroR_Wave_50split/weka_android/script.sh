logcat TestLog:I \*:S > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
# http://stackoverflow.com/questions/24639338/how-to-obtain-trace-of-methods-called-by-android-app-on-main-activity-launch-wi
am start -n com.example.andrea.wekaandroidport/.MainActivity

# Choose J48
tapnswipe /dev/input/event1 tap 450 1050
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 6500000

# Train 50/50
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 840
microsleep 200000

# Validate 50/50
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 1080
microsleep 300000
# microsleep 1000000

# Idle time
kill $PID
{{{timing}}}
