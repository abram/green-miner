# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n sonic.dash.guide/sonic.dash.guide.activities.MainActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 433 414 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 144 951 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 580 675 1000
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 416 998 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 339 308 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 110 445 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 551 693 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 167 729 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 517 966 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 388 1007 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 100 100 100 1000 2000
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME