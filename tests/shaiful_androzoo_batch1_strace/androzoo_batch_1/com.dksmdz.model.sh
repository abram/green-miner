# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.dksmdz.model/com.dksmdz.model.LoadActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 100 2000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 100 2000
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 1000 2000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 325 332 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 90 803 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 64 219 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 635 239 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 442 736 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 154 372 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 436 1026 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 653 256 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 169 918 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 592 547 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 500 1085 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 335 897 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 679 747 300
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 380 292 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME