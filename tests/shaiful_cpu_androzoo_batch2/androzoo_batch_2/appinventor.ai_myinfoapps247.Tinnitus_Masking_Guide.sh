# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n appinventor.ai_myinfoapps247.Tinnitus_Masking_Guide/appinventor.ai_myinfoapps247.Tinnitus_Masking_Guide.Screen1
microsleep 10000000
{{{timing}}}
###### tap ##########
tapnswipe /dev/input/event1 tap 569 747
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 71 1093
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 558 354
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 252 900
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 331 1049
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 244 595
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 541 688
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 218 965
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 59 1006
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 486 595
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 494 1003
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 556 1021
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 557 1068
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 100
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 231 982
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 50 283
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 260 824
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 289 976
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 669 177
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 534 681
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 20 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 120 1051
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 620 444
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME
