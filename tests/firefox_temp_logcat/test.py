import libgreenminer,time,subprocess
class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		run.phone.shell('settings put system screen_off_timeout 1800000').strip()
		
		# Clear Log
		run.phone.shell("logcat -v time -c")

	def after(self, run):
		# Reset Screen Timeout
		run.phone.shell('settings put system screen_off_timeout ' + self.timeout)
		time.sleep(2)

		# Get firefox PID
		self.pid = run.phone.shell("ps | grep mozilla").split()[1]

	def before_upload(self,run):
		# Get Log
		run.phone.shell("logcat -v time -d | grep -e "+ self.pid.strip() +" -e mozilla > /sdcard/logcat.txt")
		
		# Kill PID
		try:
			pid = run.phone.shell("ps | grep mozilla").split()[1]
			run.phone.shell("su -c 'kill "+ pid + "'")
		except:
			pass

		# Put the files into the folder before uploading
		run.phone.adb("pull /sdcard/logcat.txt "+run.wattlog_file+"_logcat.txt")
		run.phone.shell("su -c 'rm /sdcard/logcat.txt' ")
