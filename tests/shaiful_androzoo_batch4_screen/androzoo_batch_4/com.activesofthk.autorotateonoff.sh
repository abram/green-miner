# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.activesofthk.autorotateonoff/com.activesofthk.autorotateonoff.MainActivity
microsleep 10000000
{{{timing}}}
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 433 1090
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 292 1072
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 172 1025
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 247 913
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 1000
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 692 763
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 461 117
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 29 533
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 100
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME