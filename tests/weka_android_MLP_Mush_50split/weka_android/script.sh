logcat TestLog:I \*:S > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n weka.mlp_neuroph/.MainActivity
# Choose MLP
tapnswipe /dev/input/event1 tap 250 910
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 7500000

# Train 50/50
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 840
microsleep 250000000

# Validate 50/50
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 1080
microsleep 1500000

# Idle time
{{{timing}}}
kill $PID
