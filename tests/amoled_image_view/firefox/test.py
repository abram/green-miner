import os, time
import libgreenminer


class Test(libgreenminer.AndroidTest):
	def push_image(self, run):

		image_dir = os.path.join(os.path.dirname(__file__), 'screenshots')
		files = (f for f in os.listdir(image_dir) if \
				run.version in f and os.path.splitext(f)[1] == '.png')

		image = os.path.join(image_dir, next(files))
		run.phone.push(image, '/sdcard/ff.png')

	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		self.brightness = run.phone.shell('settings get system screen_brightness').strip()
		run.phone.shell('settings put system screen_off_timeout 1800000')
		run.phone.shell('settings put system screen_brightness 255')
		run.phone.shell('su -c pm enable com.google.android.gallery3d')
		self.push_image(run)

	def after(self, run):
		# Reset Screen Timeout
		run.phone.shell('su -c am kill com.google.android.gallery3d')
		time.sleep(2)
		print('kill attempted')
		run.phone.shell('su -c pm disable com.google.android.gallery3d')
		run.phone.shell('settings put system screen_off_timeout ' + self.timeout)
		run.phone.shell('settings put system screen_brightness' + self.brightness)

	def clean_files(self, phone):
		super().clean_files(phone)
		phone.shell('rm /sdcard/ff.png')
