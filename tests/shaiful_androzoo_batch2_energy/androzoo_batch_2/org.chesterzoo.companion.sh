# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n org.chesterzoo.companion/org.chesterzoo.companion.activities.MasterActivity
microsleep 10000000
{{{timing}}}
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 605 710
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 110 451
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 344 237
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 100
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 681 609
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 646 624
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 674 1011
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 470 809
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 164 977
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 327 980
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 605 1012
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 496 691
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 621 241
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME