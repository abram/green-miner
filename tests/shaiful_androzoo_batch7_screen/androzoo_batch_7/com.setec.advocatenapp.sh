# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.setec.advocatenapp/com.setec.advocatenapp.AdvocatenAppActivity
microsleep 10000000
{{{timing}}}
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 20 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 241 324
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 86 791
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 672 988
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 172 173
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 113 833
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 565 191
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 586 350
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 201 517
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 342 871
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 436 919
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 594 1025
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 20 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 218 651
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME