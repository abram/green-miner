# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n drwebsterapps.giants.schedule/drwebsterapps.giants.schedule.TriviaMainMenuActivity
microsleep 10000000
{{{timing}}}
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 323 110
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 373 56
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 523 411
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 537 410
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 20 20
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 695 20
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 33 895
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 228 657
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 70 838
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 682 244
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 179 1006
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 422 857
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 647 859
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 122 477
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 588 836
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 348 583
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 313 791
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 340 995
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME