# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n air.com.cratergames.game3072/air.com.cratergames.game3072.AppEntry
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 28 151
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 437 498
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 467 730
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 662 114
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 100
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 100
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 166 308
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 432 838
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 222 727
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 22 383
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 338 918
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 544 166
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 692 452
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 208 373
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 33 959
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 270 295
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 676 572
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 434 232
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 377 986
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 479 762
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 362 1029
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME
