# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.mobligation.timberbrowser.theme.gray/com.mobligation.timberbrowser.theme.gray.MainActivity
microsleep 10000000
{{{timing}}}
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 257 749
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 462 465
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 20 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 636 321
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 177 788
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 22 975
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 382 207
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 685 544
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 35 610
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 66 190
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 290 880
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 20 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 570 240
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 254 740
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 1000
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 304 960
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME