# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.tolkein.stratus/com.tolkein.stratus.Splash
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 544 678
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 383 284
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 416 279
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 537 1063
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 146 177
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 100
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 596 159
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 230 189
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 660 392
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 395 586
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 415 145
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 245 262
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 695 1000
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 271 1082
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 274 497
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 487 469
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 664 473
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME