# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n org.indywidualni.fblite/org.indywidualni.fblite.MainActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 135 949 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 518 1006 300
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 421 501 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 417 393 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 tap 615 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 403 700 300
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 356 258 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 615 1100 100 1100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 340 873 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 189 377 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME