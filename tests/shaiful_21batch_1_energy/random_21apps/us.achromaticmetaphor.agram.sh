# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n us.achromaticmetaphor.agram/us.achromaticmetaphor.agram.MainActivity
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 607 859 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 189 467 300
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 tap 615 1100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 554 940 300
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 346 1080 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 615 100 615 1100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 257 151 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 615 100 100 100 2000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 157 864 1000
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME