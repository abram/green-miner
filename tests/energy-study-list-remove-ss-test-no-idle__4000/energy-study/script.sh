# Wait for wattlog
microsleep 10000000

# Just setup and teardown overhead
{{{timing}}}
sh /sdcard/suites/suite_setup_and_teardown.sh >> /sdcard/testlog 2>&1 

# Remove from beginning of ArrayList
{{{timing}}}
sh /sdcard/suites/suite_al_rem_beg.sh >> /sdcard/testlog 2>&1 

# Remove from beginning of LinkedList
{{{timing}}}
sh /sdcard/suites/suite_ll_rem_beg.sh >> /sdcard/testlog 2>&1 

# Remove from beginning of TreeList
{{{timing}}}
sh /sdcard/suites/suite_tl_rem_beg.sh >> /sdcard/testlog 2>&1 

# Remove from middle of ArrayList
{{{timing}}}
sh /sdcard/suites/suite_al_rem_mid.sh >> /sdcard/testlog 2>&1  

# Remove from middle of LinkedList
{{{timing}}}
sh /sdcard/suites/suite_ll_rem_mid.sh >> /sdcard/testlog 2>&1  

# Remove from middle of TreeList
{{{timing}}}
sh /sdcard/suites/suite_tl_rem_mid.sh >> /sdcard/testlog 2>&1 

# Remove from end of ArrayList
{{{timing}}}
sh /sdcard/suites/suite_al_rem_end.sh >> /sdcard/testlog 2>&1 

# Remove from end of LinkedList
{{{timing}}}
sh /sdcard/suites/suite_ll_rem_end.sh >> /sdcard/testlog 2>&1 

# Remove from end of TreeList
{{{timing}}}
sh /sdcard/suites/suite_tl_rem_end.sh >> /sdcard/testlog 2>&1  

# idle time 
{{{timing}}}

