# redirect stdout(1) and stderr(2) to null:
exec 1>/dev/null 2>/dev/null

# Wait for Wattlog
microsleep 10000000

sh /sdcard/timing.sh "Load App & Login"
# Load App
am start -n com.evernote/com.evernote.ui.HomeActivity
microsleep 5000000
# Sign In
tapnswipe /dev/input/event1 tap 690 100
microsleep 1000000
{{#AUTH}}{{#evernote}}
# Username
tapnswipe /dev/input/event1 tap 360 237
microsleep 1000000
input text {{username}}
# Password
tapnswipe /dev/input/event1 tap 360 313
microsleep 1000000
input text {{password}}
{{/evernote}}{{/AUTH}}
# Enter
tapnswipe /dev/input/event1 tap 260 410

# Wait for load/sync
microsleep 10000000

# press back button to return to main evernote menu
tapnswipe /dev/input/event1 tap 139 1250
microsleep 1000000

sh /mnt/sdcard/timing.sh "Start Note"
# create note
tapnswipe /dev/input/event1 tap 160 350
microsleep 2500000

sh /mnt/sdcard/timing.sh "Idle Empty Note"
microsleep 20000000

sh /mnt/sdcard/timing.sh "Compose Note"
# Title
input text awesome%stest%snote
# Note
tapnswipe /dev/input/event1 tap 160 750
microsleep 1000000
input text "Williamsburg%siPhone%strust%sfund%scornhole%spork%sbelly%sreprehenderit%sphoto%sbooth,%sforage%sfixie%sintelligentsia%swolf.%sbanjo%sminim%skeffiyeh%sid%sfarm-to-table%svinyl.%sBeard%stote%sbag%ssriracha%slaboris,%stempor%sTerry%sRichardson%sorganic%sbespoke%sput%sa%sbird%son%sit%sBrooklyn%sassumenda.%sPour-over%sadipisicing%sVice%ssemiotics.%sChillwave%sNeutra%sBrooklyn%sethical%sart%sparty.%sBiodiesel%senim%syou%sprobably%shaven't%sheard%sof%sthem,%ssquid%saccusamus%s8-bit%swolf%sculpa%sfarm-to-table%svegan%splaceat%spop-up.%sAesthetic%slomo%shashtag,%sBanksy%sexcepteur%sasymmetrical%seu%sOdd%sFuture."
microsleep 2000000

sh /mnt/sdcard/timing.sh "Idle Complete Note"
microsleep 20000000

sh /mnt/sdcard/timing.sh "Save Note"
# Save
tapnswipe /dev/input/event1 tap 35 100
# Wait for Sync
microsleep 10000000

sh /mnt/sdcard/timing.sh "Delete Note"
# go to notes
tapnswipe /dev/input/event1 tap 160 845
microsleep 1000000
# long press note
tapnswipe /dev/input/event1 tap 160 312 2000
microsleep 1000000
# press delete
tapnswipe /dev/input/event1 tap 160 1060
microsleep 1000000
# press ok
tapnswipe /dev/input/event1 tap 520 760
microsleep 1000000

# "Exit" Process
sh /sdcard/timing.sh "Exit & Wait"
microsleep 2000000
tapnswipe /dev/input/event1 tap 385 1250
