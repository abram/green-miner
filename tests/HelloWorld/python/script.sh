set -e

# Wait for Wattlog
~/bin/microsleep 5000000

{{{ timing }}}

# Run app
python hello.py > /tmp/hello_python

{{{ timing }}}

# Cleanup
rm -rf /tmp/hello_python
