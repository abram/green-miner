# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n weacode.game.kilo/org.cocos2dx.cpp.AppActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent 61
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 4000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 4000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 695 1150
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 564 72
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 228 499
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 345 677
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 198 263
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 483 338
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 20 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 419 588
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 191 830
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 264 1136
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 670 355
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 20 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 666 722
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME