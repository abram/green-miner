logcat > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n com.example.andrea.wekaandroidport/.MainActivity
# Choose SMO
tapnswipe /dev/input/event1 tap 550 1050
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 10000000

# Train CV10
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 360
microsleep 150000

# Validate CV10
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 600
microsleep 1300000

# Idle time
kill $PID
{{{timing}}}
