import libgreenminer

class Test(libgreenminer.AndroidTest):
	def after(self, run):
		# Get Screenshot
		run.phone.adb("pull /sdcard/screen.png ../pizza/" + run.version + ".png")
		run.phone.shell("rm /sdcard/screen.png")
