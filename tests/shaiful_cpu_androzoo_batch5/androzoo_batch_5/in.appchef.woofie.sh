# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n in.appchef.woofie/in.appchef.soundfie.SplashActivity
microsleep 10000000
{{{timing}}}
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 31 837
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 237 224
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 162 787
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 239 127
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 131 848
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 378 622
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 187 952
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 20 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 33 690
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 527 568
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 27 428
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 343 962
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 99 410
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 315 595
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 242 1019
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 100
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 321 404
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 207 649
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 253 918
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 622 765
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 239 1057
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 430 863
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 391 865
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME