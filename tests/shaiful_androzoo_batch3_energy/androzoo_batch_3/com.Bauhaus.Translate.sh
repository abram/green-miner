# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.Bauhaus.Translate/com.Bauhaus.Translate.activities.SplashActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 155 909
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 345 113
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 617 624
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 371 897
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 190 254
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 25 980
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 301 106
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 341 1039
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 29 409
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 245 697
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 233 416
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 543 436
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 20 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 256 785
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 86 636
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 429 944
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 490 651
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 241 885
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 304 268
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 382 478
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 148 502
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME