# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.nelson.t9interpretertp/com.nelson.t9interpretertp.MainActivity
microsleep 8000000
{{{timing}}}
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 338 297 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 1000 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 245 355 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 20 1000 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 144 184 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100 300
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 361 791 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 332 836 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 506 728 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 414 853 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 234 998 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 625 879 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME