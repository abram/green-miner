import libgreenminer

class Test(libgreenminer.AndroidTest):
	def after(self, run):
		# Get Screenshot
		run.phone.adb("pull /sdcard/screen_wikipedia.png " + run.version + ".png")
		run.phone.shell("rm /sdcard/screen_wikipedia.png")
