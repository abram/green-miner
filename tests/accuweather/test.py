import libgreenminer

class Test(libgreenminer.AndroidTest):
    def before(self, run): 

        # Set Screen Timeout
        self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
        run.phone.shell('settings put system screen_off_timeout 1800000').strip()

        # Turn off Airplane mode/make sure it's off
        run.phone.shell('settings put global airplane_mode_on 0')
        run.phone.shell('am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false')

    def after(self, run):

        # Reset Screen Timeout
        run.phone.shell('settings put system screen_off_timeout ' + self.timeout)

        # Obtain screenshot
        #run.phone.adb("pull /sdcard/accuweather.png " + run.version + ".png")
        #run.phone.shell("rm /sdcard/accuweather.png")

	# Turn Airplane mode back on
        run.phone.shell('settings put global airplane_mode_on 1')
        run.phone.shell('am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true')

        # Turn wifi back on
        #run.phone.shell('svc wifi enable') - This doesn't work for some reason
        run.phone.shell('am start -a android.intent.action.MAIN -n com.android.settings/.wifi.WifiSettings')
        run.phone.shell('sleep 2')
        run.phone.shell('input tap 620 100')
        run.phone.shell('sleep 2')
        run.phone.shell('input keyevent HOME')

        # Settings app kills itself
        #run.phone.shell('input tap 550 1250')
        #run.phone.shell('sleep 2')
        #run.phone.shell('tapnswipe /dev/input/event1 tap 450 1000 2000')
        #run.phone.shell('input tap 430 810')
