import libgreenminer

class Test(libgreenminer.AndroidTest):
    def before(self, run):
        # Set Screen Timeout
        self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
        run.phone.shell('settings put system screen_off_timeout 1800000')

    def after(self, run):
        # Reset Screen Timeout
        run.phone.shell('settings put system screen_off_timeout ' + self.timeout)
        # Obtain screenshot
        #run.phone.adb("pull /sdcard/weatherbug.png " + run.version + ".png")
        #run.phone.shell("rm /sdcard/weatherbug.png")
