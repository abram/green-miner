# weatherbug current weather test for Edmonton, Alberta, Canada

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Load App
{{{timing}}}
am start -n com.aws.android/.app.ui.HomeActivity
microsleep 10000000

# Agree to terms and conditions
tapnswipe /dev/input/event1 tap 285 1110
microsleep 2000000

# Navigate to the current weather for Edmonton

# Skip app intro
tapnswipe /dev/input/event1 tap 225 1125 
microsleep 10000000

# Enter city: Edmonton
input text 'Edmonton,%sAlberta,%sCanada'
microsleep 3000000

# Click Edmonton
tapnswipe /dev/input/event1 tap 360 210
microsleep 3000000

# Click to save location
tapnswipe /dev/input/event1 tap 250 860
microsleep 15000000

# Checkout the current weather/let app idle 
{{{timing}}}
microsleep 10000000

# Take a screencap of current weather
#screencap -p /sdcard/weatherbug.png
microsleep 10000000

# return to home
{{{timing}}}
input keyevent HOME
