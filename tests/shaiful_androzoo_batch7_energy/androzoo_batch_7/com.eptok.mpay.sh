# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.eptok.mpay/com.eptok.mpay.activity.MainGuideActivity
microsleep 10000000
{{{timing}}}
###### tap ##########
tapnswipe /dev/input/event1 tap 408 675
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 625 582
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 386 348
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 20 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 316 708
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 20 20
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 279 25
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 151 425
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 647 152
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 403 331
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 695 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 166 1142
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 326 1027
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 684 781
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 20 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 476 26
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 20 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 233 367
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 420 279
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 493 531
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 695 1150
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME