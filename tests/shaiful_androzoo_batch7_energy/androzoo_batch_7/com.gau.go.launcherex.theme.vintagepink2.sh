# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.gau.go.launcherex.theme.vintagepink2/com.gau.go.launcherex.theme.vintagepink2.NotificationActivity
microsleep 10000000
{{{timing}}}
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 343 122
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 83 336
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 178 385
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 314 745
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 695 1150
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 20 20
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 695 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 79 886
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 20 20
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 587 440
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 525 596
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 43 850
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 268 430
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 20 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 374 1123
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 76 1051
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 20 1150
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 695 1150
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 48 570
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 695 194
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME