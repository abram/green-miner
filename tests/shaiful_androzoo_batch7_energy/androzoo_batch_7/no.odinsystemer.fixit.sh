# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n no.odinsystemer.fixit/no.odinsystemer.fixit.activity.MainActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent 61
microsleep 4000000
input keyevent ENTER
microsleep 4000000
input keyevent ENTER
microsleep 4000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 4000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 4000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 597 1044
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 296 181
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 78 59
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 80 552
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 30 190
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 695 1150
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 430 650
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 618 876
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 395 741
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 20 1150
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 70 648
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 624 1121
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 820
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 106 500
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 695 20
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME