import libgreenminer,time,subprocess
import os

class MultiTestFile(libgreenminer.TestFile):
    """
    Represents a test on the file system
    """

    def __init__(self, testFile, package_name):
        """ Instantiates an object for the given path and app."""
        self.path = testFile.path
        self.app = testFile.app
        self.name = os.path.split(testFile.path)[1]
        self.package_name = package_name
        #self.package_name = self.version  # todo lookup packagename in dictionary
        
        self.py_file = os.path.join(self.path, self.app, 'test.py')


    def get_partition_file(self):
        """Gets the path to the partition_info.csv file, if it exists."""
        partition_file = os.path.join(self.path, 'partition', self.package_name + '_partition.csv')
        return partition_file if os.path.exists(partition_file) else None

    def openDurationFile(self):
        """Opens and returns the duration file for the test."""
        return open(os.path.join(self.path, 'duration', self.package_name + '_duration'), 'r')

    def openBashInFile(self):
        """Opens and returns the bash input file for the test."""
        return open(os.path.join(self.path, self.app, self.package_name + '.sh'), 'r')
    # note: '__' used to join the version with the suffix is also in the libgreenminer/result.py file. It is controller by the multiTest and version parameter in the call to TestResult()

def to_package_name(version):
    # TODO parse dictionary version => package_name
    # TODO return translated value
    return version
    
class Test(libgreenminer.AndroidTest):

        def __init__(self, test, version):  # input to our script
                self.test = test
                # TODO replace version in MultiTestFile with package_name
                self.package_name = to_package_name(version)
                self.test = MultiTestFile(test, self.package_name)
                self.duration = self.get_duration()


        def compute_statistics(self, testRun):
                """Compute the post-run statistics."""
                result = libgreenminer.TestResult(testRun.wattlog_file, multiTest=True, package_name=self.package_name)
                out_file = testRun.wattlog_file + '_totals.csv'
                with open(out_file, 'w') as f:
                        f.write(result.getTotalInfo())

                out_file = testRun.wattlog_file + '_partitions.csv'
                with open(out_file, 'w') as f:
                        f.write(result.getPartitionInfo())

                
        # todo translate version name into android package name.
