logcat TestLog:I \*:S > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n com.example.andrea.wekaandroidport/.MainActivity
# Choose IBk
tapnswipe /dev/input/event1 tap 150 770
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 83000000

# Train 50/50
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 840
microsleep 500000

# Validate 50/50
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 1080
# Split up timing to avoid overflow on microsleep
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1650000000

# Idle time
{{{timing}}}
kill $PID

# am instrument -w -e class com.example.andrea.wekaandroidport.IBk3.IBK3MNISTTest#testReadActivity,com.example.andrea.wekaandroidport.IBk3.IBK3MNISTTest#testTrain50Activity,com.example.andrea.wekaandroidport.IBk3.IBK3MNISTTest#testValidation50Activity com.example.andrea.wekaandroidport.test/android.test.InstrumentationTestRunner
