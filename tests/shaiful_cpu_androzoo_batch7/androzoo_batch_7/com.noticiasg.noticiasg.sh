# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.noticiasg.noticiasg/com.noticiasg.noticiasg.MainActivity
microsleep 10000000
{{{timing}}}
###### tap ##########
tapnswipe /dev/input/event1 tap 159 291
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 20 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 122 665
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 510 375
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 275 855
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 379 151
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 532 54
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 467 678
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 267 560
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 374 961
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 512 790
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 695 1150
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 695 20
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 695 20
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 20 1150
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 392 110
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 576 206
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 292 680
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 235 303
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 115 351
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1150 20 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 105 419
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 23 292
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 20 695 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 46 27
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME