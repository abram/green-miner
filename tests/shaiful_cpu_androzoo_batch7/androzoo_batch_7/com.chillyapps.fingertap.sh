# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.chillyapps.fingertap/com.chillyapps.fingertap.android.AndroidLauncher
microsleep 10000000
{{{timing}}}
###### tap ##########
tapnswipe /dev/input/event1 tap 263 329
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 106 404
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 367 208
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 476 853
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 125 211
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1150 20 20
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 354 502
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 244 504
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 310 678
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 134 939
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 322 466
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 397 996
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 616 134
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 602 604
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 124 178
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 20 695 20
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 263 384
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 445 88
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 388 41
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 129 652
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 325 873
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 45 616
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 661 866
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME