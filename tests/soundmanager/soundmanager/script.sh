PACKAGE="com.roozen.SoundManagerv2"
ACTIVITY="com.roozen.SoundManagerv2.MainSettings"

# Wait for Wattlog
microsleep 10000000

# Launch app ("Launch app" entry in partition_info.csv file)
{{{timing}}}
am start -n $PACKAGE/$ACTIVITY
microsleep 10000000

# Interaction ("interaction" entry in partition_info.csv file)
{{{timing}}}

# Click "close"
tapnswipe /dev/input/event1 tap 358 1004
microsleep 1000000

# Click "menu"
tapnswipe /dev/input/event1 tap 649 1227
microsleep 1000000

# Click "mute"
tapnswipe /dev/input/event1 tap 338 861
microsleep 1000000

# Click "Mute/Unmute"
tapnswipe /dev/input/event1 tap 214 612
microsleep 1000000

# Click to open applications list
tapnswipe /dev/input/event1 tap 556 1242
microsleep 2000000

# Close the app
tapnswipe /dev/input/event1 swipe 225 1030 587 1030 1000
microsleep 3000000

# begin exit.
{{{timing}}}
am force-stop $PACKAGE
pm clear $PACKAGE