# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.jisshow.app/com.jisshow.app.SplashActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 510 235
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 694 971
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 421 250
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 650 442
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 627 427
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 361 121
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 160 961
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 353 337
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 128 554
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 20 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 324 1057
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 631 222
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 308 561
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 527 648
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 573 282
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 133 785
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME