# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.vsporto.dawgsportsradio/com.vsporto.dawgsportsradio.MainActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 369 475
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 495 473
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 131 960
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 298 266
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME