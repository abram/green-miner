# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.subsplash.thechurchapp.parksidebaptistchurch/com.subsplash.thechurchapp.SplashActivity
microsleep 10000000
{{{timing}}}
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 353 664
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 518 491
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 132 749
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 50 745
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 415 167
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 692 778
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 633 851
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 494 716
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 125 546
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 628 194
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 398 162
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 20 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 159 1088
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 550 1015
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 138 200
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 651 989
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 229 649
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 510 312
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 464 861
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 695 100
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 20 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 269 391
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 153 796
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 452 886
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 599 159
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 167 797
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 695 100
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME