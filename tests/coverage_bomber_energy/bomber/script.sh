# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
am start -n org.beide.bomber/org.beide.bomber.BomberActivity
microsleep 7500000

{{{timing}}}
### to start the game
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 4000000
###### tap ##########
tapnswipe /dev/input/event1 tap 229 365 300
microsleep 4000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 50 50 665 50 2000
microsleep 4000000
###### tap ##########
tapnswipe /dev/input/event1 tap 170 1018 300
microsleep 4000000
###### key event ##########
input keyevent ENTER
microsleep 4000000
###### long press ##########
tapnswipe /dev/input/event1 tap 50 125 1000
microsleep 4000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 665 1150 50 50 2000
microsleep 4000000
###### long press ##########
tapnswipe /dev/input/event1 tap 335 1034 1000
microsleep 4000000
###### tap ##########
tapnswipe /dev/input/event1 tap 413 994 300
microsleep 4000000
###### tap ##########
tapnswipe /dev/input/event1 tap 358 239 300
microsleep 4000000
# "Exit" Process
{{{timing}}}
microsleep 5000000
tapnswipe /dev/input/event1 tap 339 1240

