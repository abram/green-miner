# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.crocodic.rajawisata/com.crocodic.rajawisata.MainActivity
microsleep 8000000
{{{timing}}}
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100 300
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 111 1100 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 475 494 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 158 871 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 627 260 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 690 685 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 69 872 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 41 299 300
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100 300
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 626 612 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 385 271 1000
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 20 1000 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 204 268 300
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 584 815 1000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 662 314 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 629 625 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 496 375 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 484 915 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 175 1040 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 426 775 1000
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME