# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.voyagerinnovation.talk2/com.voyagerinnovation.talk2.registration.RegistrationActivity
microsleep 8000000
{{{timing}}}
###### long press ##########
tapnswipe /dev/input/event1 tap 95 830 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 535 256 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 32 205 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 148 708 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 573 208 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME