# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n eu.liveclick.aja1/eu.liveclick.aja1.main
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 355 153 300
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 392 1046 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 53 390 1000
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 95 573 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 1000 2000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 52 356 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 481 805 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 174 936 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 432 230 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 411 667 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 100 2000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 20 1000 2000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 489 835 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 475 249 300
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 329 432 300
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 100 2000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 191 700 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 597 746 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME