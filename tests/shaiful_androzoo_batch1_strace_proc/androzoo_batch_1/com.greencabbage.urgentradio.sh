# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.greencabbage.urgentradio/com.greencabbage.patch.Splash
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 680 1085 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 410 650 300
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 570 543 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 344 403 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 300 689 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100 300
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 120 673 1000
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME