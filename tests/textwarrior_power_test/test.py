import libgreenminer,time,subprocess


class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		run.phone.shell('settings put system screen_off_timeout 900000').strip()

	def after(self, run):
		# Reset Screen Timeout
		run.phone.shell('settings put system screen_off_timeout ' + self.timeout)
                run.phone.shell('rm "/sdcard/wartext"')
                run.phone.shell('rm /sdcard/timing')

	def before_upload(self,run):
		#Put the files into the folder before uploading
		try:
			pid = run.phone.shell("ps | grep warrior").split()[1]
			run.phone.shell("su -c 'kill "+ pid + "'")
		except:
			pass

		#run.phone.adb("pull /data/local/trc.txt "+run.wattlog_file+"_strace.txt")
		#run.phone.adb("pull /sdcard/java.trace "+run.wattlog_file+"_java.trace")
        
	#def clean_files(self, phone):
		#super().clean_files(phone)
		#phone.shell('rm "/sdcard/wartext"')
		
