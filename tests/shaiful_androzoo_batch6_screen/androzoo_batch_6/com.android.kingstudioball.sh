# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.android.kingstudioball/com.android.kingstudioball.GameSelection.ActivityMain
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 493 978
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 258 897
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 177 859
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 100
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 100
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 634 523
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 630 552
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 178 344
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 423 381
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 26 111
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 692 710
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 456 1015
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 440 462
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 682 294
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 323 691
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME