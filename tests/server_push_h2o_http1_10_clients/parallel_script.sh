#!/bin/bash

LOGFILE=/tmp/parallel_script.log
CLIENTLOGFILE=/tmp/client_requests.log

echo "$(date "+%m%d%Y %T") : Script Started. Going to sleep for 30 seconds" >> $LOGFILE 2>&1
#Sleep for 40 seconds.
sleep 30

echo "$(date "+%m%d%Y %T") : 30 Seconds Sleep over. Starting parallel command" >> $LOGFILE 2>&1

#seq 50 | parallel -j10 -n0 --joblog /home/intel/workspace/http.client/target/joblog  java -jar /home/intel/workspace/http.client/target/httpasynchronousclient.jar /home/intel/workspace/http.client/target/all_urls.txt  > /dev/null 2>&1

#seq 10 | parallel -j1 -n0 --joblog /tmp/joblog java -jar /home/pi/green-star/tests/server_push_h2o_http1_10_clients/httpasynchronousclient.jar /home/pi/green-star/tests/server_push_h2o_http1_10_clients/all_urls.txt > /dev/null 2>&1 & echo $! > /tmp/httpclient.pid

#with joblog and client log
seq 10 | parallel -j1 -n0 --joblog /tmp/joblog java -jar /home/pi/green-star/tests/server_push_h2o_http1_10_clients/httpasynchronousclient.jar /home/pi/green-star/tests/server_push_h2o_http1_10_clients/all_urls.txt >> $CLIENTLOGFILE 2>&1 & echo $! > /tmp/httpclient.pid

echo "$(date "+%m%d%Y %T") : Parallel command ended." >> $LOGFILE 2>&1
