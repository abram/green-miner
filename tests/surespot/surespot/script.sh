# Surespot test for sending 15 messages using one device

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Lauch app 
{{{timing}}} 
am start -n com.twofours.surespot/.activities.MainActivity
microsleep 9000000

# Restore identity
tapnswipe /dev/input/event1 tap 610 190
microsleep 5000000

# Choose your identity
# alannabanana666
tapnswipe /dev/input/event1 tap 360 610
microsleep 4000000

# Enter password
input text 'password'
microsleep 1000000

# Click login
tapnswipe /dev/input/event1 tap 510 820
microsleep 15000000

# Enter password again!
input text 'password'
microsleep 1000000

# Click login again!
tapnswipe /dev/input/event1 tap 370 630
microsleep 15000000

# Close popup 1
tapnswipe /dev/input/event1 tap 320 1110
microsleep 13000000

# Close popup 2
tapnswipe /dev/input/event1 tap 25 100
microsleep 15000000

# Click to chat with lizard person 
tapnswipe /dev/input/event1 tap 360 270
microsleep 10000000

# Only phone D will send messages
SERIAL={{{serial}}}
if [ "$SERIAL" = "01498B1C0100F00B" ]; then

	# Send some msgs to Lizard Person
	{{{timing}}}
	# Message 1
	input text 'IM%sis%sa%stype%sof%sonline%schat%swhich%soffers%sreal-time...'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000

	# Message 2
	input text '...text%stransmission%sover%sthe%sInternet.%sShort%smessages%sare...'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000

	# Message 3
	input text '...transmitted%sbi-directionally%sbetween%stwo%sparties.'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000

	# Message 4
	input text 'Some%sexample%stexts:'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000
	
	# Message 5
	input text 'Omg%s!!!!%sAre%syou%sserious'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000

	# Message 6
	input text 'lol'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000
	
	# Message 7
	input text 'Okay%shaha'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000
	
	# Message 8
	input text 'No%sworries!!!!!'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000

	# Message 9
	input text 'I%sjust%snoticed%sthat%syour%semail%ssaid%s2%ssorry'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000
	
	# Message 10
	input text 'false%salarm.%sDont%stell%shillary%sanything!'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000

	# Message 11
	input text 'Oh%sno%ssorry%sI%sthink%sI%saccidentally%ssent%sa%sblank%stext'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000

	# Message 12
	input text 'k'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000

	# Message 13
	input text 'Argh%sI%sforgot%sto%sreply%sfor%ssure%sthis%ssounds%sgreat'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000
	
	# Message 14
	input text 'Oh%sgeez%sthat%sis%sso%strue...%sYugh'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000

	# Message 15
	input text 'bye'
	microsleep 2000000
	# Send
	tapnswipe /dev/input/event1 tap 670 1140
	microsleep 2000000
fi
	
# Let app idle
{{{timing}}}
microsleep 20000000
	
# Return to home
{{{timing}}}
input keyevent HOME
