#
# Blocking Test
#	Loads a webpage that spits out 1024 white spaces, then random characters
#	as follows:
#	http://pizza.cs.ualberta.ca:2222/1/120/25
#		'1'		: delay between lines (in seconds)
#		'120'	: number of lines to print
#		'25'	: number of characters per line
#
# Copyright (c) 2013 Jed Barlow, Kent Rasmussen, Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not see <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App & Enter URL
{{{timing}}}
am start -n org.mozilla.firefox/.App
microsleep 12000000
# Don't send data
tapnswipe /dev/input/event1 tap 553 376

# Click on address bar, enter URL
tapnswipe /dev/input/event1 tap 400 100
microsleep 2500000
input text "pizza.cs.ualberta.ca:2222/1/120/25"
tapnswipe /dev/input/event1 tap 667 119

# Load page
{{{timing}}}
microsleep 170000000

# "Exit" Process
{{{timing}}}
microsleep 2000000
input keyevent HOME
