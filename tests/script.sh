#
# Mozilla nightly to test http 1 performance
#	
#
# Copyright (c) 2015 Shaiful Alam Chowdhury
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
am start -n {{APP}}/.App
microsleep 10000000


#### setting http 1 and writing web address 
{{{timing}}}

tapnswipe /dev/input/event1 tap 82 96   
input text "about:config"
input keyevent ENTER

microsleep 2000000
tapnswipe /dev/input/event1 tap 419 199   
input text "spdy.enabled"
microsleep 1000000

### network.http.spdy.enabled false
tapnswipe /dev/input/event1 tap 617 389
microsleep 1000000
tapnswipe /dev/input/event1 tap 617 389
microsleep 1000000

#### network.http.spdy.enabled.http2 false


tapnswipe /dev/input/event1 tap 646 790
microsleep 1000000
tapnswipe /dev/input/event1 tap 646 790
microsleep 1000000


#### network.http.spdy.enabled.http2draft false

tapnswipe /dev/input/event1 tap 637 996
microsleep 1000000
tapnswipe /dev/input/event1 tap 637 996
microsleep 1000000


tapnswipe /dev/input/event1 tap 82 96   
input text "https://pizza.cs.ualberta.ca:1801"


##### press enter and load and view five flags#################
{{{timing}}}
input keyevent ENTER
microsleep 20000000


microsleep 10000000

tapnswipe /dev/input/event1 tap 689 592
microsleep 10000000

tapnswipe /dev/input/event1 tap 689 592
microsleep 10000000

tapnswipe /dev/input/event1 tap 689 592
microsleep 10000000

tapnswipe /dev/input/event1 tap 689 592
microsleep 10000000


##### hit home button##########
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240


