# 1Weather current weather test for Edmonton, Alberta, Canada

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Load App
{{{timing}}}
am start -n com.handmark.expressweather/.MainActivity
microsleep 17000000

# Navigate to the current weather for edmonton

# Close weather fact
tapnswipe /dev/input/event1 tap 110 730 
microsleep 1000000
tapnswipe /dev/input/event1 tap 360 850
microsleep 4000000

# Search for Edmonton
input text 'Edmonton,%sAlberta,%sCanada'
microsleep 4000000

# Screen may auto rotate here 

# For phones on their side: Click Edmonton
tapnswipe /dev/input/event1 tap 520 650
microsleep 15000000

# For phones face up: Click Edmonton
#tapnswipe /dev/input/event1 tap 360 230
#microsleep 15000000

# Click Edmonton again
tapnswipe /dev/input/event1 tap 360 300
microsleep 2000000

# Close fullscreen ad
tapnswipe /dev/input/event1 tap 25 25
microsleep 5000000 

# Checkout the current weather/let app idle) 
{{{timing}}}
microsleep 20000000

# Take a screencap of current weather
#screencap -p /sdcard/1weather.png

# return to home
{{{timing}}}
input keyevent HOME
