import libgreenminer

class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		run.phone.shell('settings put system screen_off_timeout 1800000').strip()

		# Enable Google Play Services
		run.phone.shell('su -c \'pm enable com.google.android.gms\'').strip()

	def after(self, run):
		# Disable Google Play Services
		run.phone.shell('su -c \'pm disable com.google.android.gms\'').strip()

		# Reset Screen Timeout
		run.phone.shell('settings put system screen_off_timeout ' + self.timeout)
