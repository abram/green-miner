# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}

am start -n de.danoeh.antennapod/de.danoeh.antennapod.activity.MainActivity
microsleep 30000000
# tap on subscribtions
tapnswipe /dev/input/event1 tap 370 452 500
microsleep 25000000
# tap on add podcast
tapnswipe /dev/input/event1 tap 260 420 500
microsleep 25000000
# tap on search itunes
tapnswipe /dev/input/event1 tap 550 642 500
microsleep 25000000
# tap on first in the list
tapnswipe /dev/input/event1 tap 150 378 500
microsleep 30000000
# tapon subscribe button
tapnswipe /dev/input/event1 tap 535 707 500
microsleep 30000000
# back
tapnswipe /dev/input/event1 tap 80 154 500
microsleep 25000000
# back
tapnswipe /dev/input/event1 tap 80 154 500
microsleep 25000000
# back
tapnswipe /dev/input/event1 tap 80 154 500
microsleep 25000000
# tap on podcast to open
tapnswipe /dev/input/event1 tap 166 414 500
microsleep 25000000
# tap on first track 
tapnswipe /dev/input/event1 tap 353 833 500
microsleep 25000000
# tap on stream to listen to the stream 
tapnswipe /dev/input/event1 tap 808 565 500
microsleep 30000000
