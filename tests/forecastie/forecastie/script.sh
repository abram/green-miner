# Forecastie application idle state for 15 seconds.

# Wait for Wattlog
microsleep 10000000

# Launch app 
{{{timing}}}
am start -n cz.martykan.forecastie com.tencent.mm/.MainActivity

# Wait
{{{timing}}}
microsleep 15000000