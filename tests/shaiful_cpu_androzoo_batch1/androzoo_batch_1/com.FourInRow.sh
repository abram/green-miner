# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.FourInRow/com.FourInRow.FourInRow
microsleep 8000000
{{{timing}}}
###### tap ##########
tapnswipe /dev/input/event1 tap 40 100 300
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 100 2000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 1000 2000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 542 575 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 107 472 1000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 464 598 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 26 429 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 523 1065 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 68 529 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 244 275 300
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME