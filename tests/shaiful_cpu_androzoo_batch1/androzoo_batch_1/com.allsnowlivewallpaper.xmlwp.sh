# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.allsnowlivewallpaper.xmlwp/com.allsnowlivewallpaper.xmlwp.Settings
microsleep 8000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent 61
microsleep 1000000
input keyevent ENTER
microsleep 1000000
input keyevent ENTER
microsleep 1000000
tapnswipe /dev/input/event1 tap 200 400 300
microsleep 2000000
tapnswipe /dev/input/event1 tap 300 500 1000
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 477 1085 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 398 831 300
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 159 813 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 tap 20 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 609 504 300
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 463 317 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 159 471 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 100 2000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 20 1000 2000
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 645 891 1000
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### long press ##########
tapnswipe /dev/input/event1 tap 674 696 1000
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 tap 20 100 2000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 45 853 300
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 100 20 1000 2000
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME