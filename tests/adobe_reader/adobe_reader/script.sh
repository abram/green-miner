# Adobe Acrobat Reader keyword test 

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Load App
{{{timing}}}
am start -n com.adobe.reader/.AdobeReader
microsleep 20000000

# Swipe through app intro
tapnswipe /dev/input/event1 swipe 650 1000 50 1000 3000
microsleep 4000000
tapnswipe /dev/input/event1 swipe 650 1000 50 1000 3000
microsleep 4000000
# 2! extra swipes to ensure we don't get trapped
tapnswipe /dev/input/event1 swipe 650 1000 50 1000 3000
microsleep 4000000
tapnswipe /dev/input/event1 swipe 650 1000 50 1000 3000
microsleep 4000000
tapnswipe /dev/input/event1 tap 360 1150
microsleep 15000000

# Find PDF
tapnswipe /dev/input/event1 tap 290 230 
microsleep 2000000

# Open PDF
tapnswipe /dev/input/event1 tap 340 790
microsleep 8000000

# Click search 
{{{timing}}}
tapnswipe /dev/input/event1 tap 550 60
microsleep 1000000

# Search for keyword
input text 'alien'
microsleep 1000000
input keyevent ENTER
microsleep 10000000

# Click first result page
{{{timing}}}
tapnswipe /dev/input/event1 tap 300 400
microsleep 2000000

# Let app idle
{{{timing}}}
microsleep 20000000

# Return home
{{{timing}}}
input keyevent HOME
