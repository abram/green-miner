import libgreenminer,time,subprocess

class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		run.phone.shell('settings put system screen_off_timeout 1800000').strip()

		# Push Sample File
		run.phone.adb("push tests/vlc/vlc/sample.3gp /sdcard/sample.3gp")

	def after(self, run):
		# Reset Screen Timeout
		run.phone.shell('settings put system screen_off_timeout ' + self.timeout)

	def before_upload(self,run):
		# Remove sample
		run.phone.shell("rm /sdcard/sample.3gp")
