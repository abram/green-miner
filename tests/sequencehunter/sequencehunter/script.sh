# Application idle state for 5 seconds.
PACKAGE="com.monead.games.android.sequence"
ACTIVITY="com.monead.games.android.sequence.Sequence"

# Wait for Wattlog
microsleep 10000000

# Launch app ("Launch app" entry in partition_info.csv file)
{{{timing}}}
am start -n $PACKAGE/$ACTIVITY
microsleep 10000000

# Interaction ("interaction" entry in partition_info.csv file)
{{{timing}}}

# Click "Play"
tapnswipe /dev/input/event1 tap 41 1041
microsleep 2000000

# fill Grid
# 1st line
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 663 1159
microsleep 500000
# 2nd line
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 663 1159
microsleep 500000
# 3rd line
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 663 1159
microsleep 500000
# 4th line
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 663 1159
microsleep 500000
# 5th line
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 663 1159
microsleep 500000
# 6th line
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 663 1159
microsleep 500000
# 7th line
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 663 1159
microsleep 500000
# 8th line
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 663 1159
microsleep 500000
# 9th line
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 663 1159
microsleep 500000
# 10th line
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 47 1132
microsleep 250000
tapnswipe /dev/input/event1 tap 663 1159
microsleep 500000

microsleep 2000000

# Click to open applications list
tapnswipe /dev/input/event1 tap 556 1242
microsleep 2000000

# Close the app
tapnswipe /dev/input/event1 swipe 225 1030 587 1030 1000
microsleep 500000

# begin exit.
{{{timing}}}
am force-stop $PACKAGE
pm clear $PACKAGE