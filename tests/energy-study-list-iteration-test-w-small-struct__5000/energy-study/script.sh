# Wait for wattlog
microsleep 10000000
 
# Just setup and teardown overhead
{{{timing}}}
sh /sdcard/suites/suite_setup_and_teardown.sh >> /sdcard/testlog 2>&1 


# Iteration over ArrayList of Points
{{{timing}}}
sh /sdcard/suites/suite_al_itr.sh >> /sdcard/testlog 2>&1 


# Iteration over LinkedList of Points
{{{timing}}}
sh /sdcard/suites/suite_ll_itr.sh >> /sdcard/testlog 2>&1 


# Iteration over TreeList Points
{{{timing}}}
sh /sdcard/suites/suite_tl_itr.sh >> /sdcard/testlog 2>&1 
 

# idle time 
{{{timing}}}

