import libgreenminer,time,subprocess

class Test(libgreenminer.AndroidTest):
	def before(self, run):
		# Set Screen Timeout
		self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
		run.phone.shell('settings put system screen_off_timeout 1800000').strip()
		run.phone.adb("push tests/vlc/vlc/sample.3gp /sdcard/sample.3gp")
		run.phone.adb("push traceTools/jtrcvlc.sh /sdcard/")
		#run.phone.adb("push traceTools/strace /sdcard/")
		run.phone.shell("su -c 'cp /sdcard/jtrcvlc.sh /data/local/' ")
		#run.phone.shell("su -c 'cp /sdcard/strace /data/local/' ")
		run.phone.shell("su -c 'rm /sdcard/jtrcvlc.sh' ")
		#run.phone.shell("su -c 'rm /sdcard/strace' ")
		#run.phone.shell("su -c 'rm /sdcard/strc.sh' ")
		#run.phone.shell("su -c 'chmod 0777 strc.sh'")
		#run.phone.shell("su -c 'chmod 0777 strace'")
		run.phone.shell("su -c 'chmod 0777 jtrcvlc.sh'")
		#run.phone.shell("sh /data/local/jtrc.sh&")
                #run.phone.shell("sh   /data/local/strc.sh&")
                subprocess.call("adb shell busybox nohup sh /data/local/jtrcvlc.sh&", shell=True)
                time.sleep(2)

	def after(self, run):
		# Reset Screen Timeout
		#run.phone.adb("pull /data/local/trc.txt /home/pi/green-star/" + run.version + ".txt")
		#run.phone.shell("rm /data/local/trc.txt")
                run.phone.shell('settings put system screen_off_timeout ' + self.timeout)
		pid = run.phone.shell("ps | grep videolan").split()[1]
		run.phone.shell("am profile " + pid + " stop")
		run.phone.shell("rm /sdcard/sample.3gp")

	def before_upload(self,run):
		run.phone.adb("pull /sdcard/java.trace "+run.wattlog_file+"_java.trace")
