#! /usr/bin/python2.7
# Generates a slow loading webpage of random text.

# HowTo:
#   Start Server:
#		python server.py
#	Run:
#		Navigate to localhost/<delay>/<lines>/<chars>.html
#			Delay -> # of seconds between printing each line
#			Lines -> # of lines to print
#			Chars -> # of characters in each line
#
# Requirements:
#	Daemonize - https://pypi.python.org/pypi/daemonize/
#		(pip install daemonize)

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from SocketServer import ForkingMixIn
from daemonize import Daemonize
import os, random, re, string, time

class HTTPRequestHandler(BaseHTTPRequestHandler):
	def do_GET(self):
		try:
			# Get Length & Delay From URL
			m = re.match(r"/(\d+)/(\d+)/(\d+).*", self.path)
			delay = int(m.group(1))
			lines = int(m.group(2))
			chars = int(m.group(3))

			# Send Response and Headers
			self.send_response(200)
			self.send_header('Content-Type', 'text/plain')
			# 1025 for line of spaces, and +1 for the newline character
			self.send_header('Content-Length', 1025 + (lines) * (chars + 1))
			self.end_headers()

			# Send whitespace to get started
			#   Why? Firefox (and other browsers) don't like to display things
			#   until the connection is ended or a certain amount of data is
			#   received. See comments at http://goo.gl/KgHG6
			self.wfile.write(' ' * 1024 + '\n')
			self.wfile.flush()
			time.sleep(delay)

			# Print specified number of lines
			for x in range(lines):
				#Print specified number of random characters (and newline)
				for y in range(chars):
					self.wfile.write(random.choice(string.ascii_uppercase))
				self.wfile.write('\n')

				# Send Line of Output
				self.wfile.flush()

				# Create a delay between each run
				time.sleep(delay)
			return

		# This shouldn't happen
		except IOError:
			self.send_error(500, 'Something went terribly wrong!')

# We don't change anything in this class, allows for multithreading
class ThreadedHTTPServer(ForkingMixIn, HTTPServer):
	pass

def main():
	print('Server is starting...')

	# Configure Server
	server_address = ('0.0.0.0', 2222)
	server = ThreadedHTTPServer(server_address, HTTPRequestHandler)

	print('Server is up & running!')
	# Start Server
	server.serve_forever()

daemon = Daemonize(app="blocking_server", pid="/tmp/blocking_server.pid", action=main)
daemon.start()
