#!/usr/bin/perl
#
# Copyright (c) 2013 Jed Barlow
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use CGI;

my $q = CGI->new;
print $q->header();

for (my $i=0; $i<10; $i++) {
    print " " unless ($i == 0);
	print 2 * (rand() - 0.5);
}
