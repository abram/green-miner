logcat > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n com.example.andrea.wekaandroidport/.MainActivity
# Choose J48
tapnswipe /dev/input/event1 tap 150 210
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 83000000

# Train CV10
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 360
microsleep 800000000

# Validate CV10
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 600
# Split microsleep so it doesn't overflow
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1000000000
microsleep 1850000000

# Idle time
{{{timing}}}
kill $PID
