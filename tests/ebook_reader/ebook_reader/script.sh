# E-book Reader (by ebook.com) test 

# Wait for Wattlog
microsleep 10000000

# Take idle measurement
{{{timing}}}
microsleep 10000000

# Load App
{{{timing}}}
am start -n com.ebooks.ebookreader/.activity.LaunchActivity
microsleep 16000000

# Close update notes
tapnswipe /dev/input/event1 tap 360 800
tapnswipe /dev/input/event1 tap 480 1020
microsleep 2000000

# Find import epub
# Click menu
tapnswipe /dev/input/event1 tap 680 100
microsleep 2000000
# Click Import 
tapnswipe /dev/input/event1 tap 520 580
microsleep 2000000
# Open Books folder
tapnswipe /dev/input/event1 tap 360 680
microsleep 2000000
# Click epub
tapnswipe /dev/input/event1 tap 360 360
microsleep 2000000
# Click import
# It is very important that epub fully imports
tapnswipe /dev/input/event1 tap 610 210
microsleep 25000000

### Let app idle
{{{timing}}}
microsleep 20000000

# Search for word "attired". Note this word only appears 1 time.
{{{timing}}}
# Exit fullscreen mode
tapnswipe /dev/input/event1 tap 360 660
microsleep 2000000
# slide menu over
tapnswipe /dev/input/event1 tap 640 1050
microsleep 2000000
tapnswipe /dev/input/event1 tap 640 1050
microsleep 2000000
# Click search
tapnswipe /dev/input/event1 tap 500 1050
microsleep 2000000
tapnswipe /dev/input/event1 tap 360 50
microsleep 1000000
input text 'attired'
microsleep 2000000
# Click search
tapnswipe /dev/input/event1 tap 670 60
microsleep 11000000
# Click for context
tapnswipe /dev/input/event1 tap 360 150
microsleep 11000000

# Prepare to read
{{{timing}}}
# Click to hide search bar
tapnswipe /dev/input/event1 tap 360 660
microsleep 1000000
# Click to enter fullscreen mode
tapnswipe /dev/input/event1 tap 360 660
microsleep 5000000
# Go to forward one page
tapnswipe /dev/input/event1 tap 680 630
microsleep 4000000

# Read for a while
{{{timing}}}
# The Stag at the Pool
microsleep 1000000
tapnswipe /dev/input/event1 tap 680 630
# The Stag at the Pool cont.
microsleep 16000000
tapnswipe /dev/input/event1 tap 680 630
# The Stag at the Pool cont.
microsleep 20000000
tapnswipe /dev/input/event1 tap 680 630
# The Fox and the Mask
microsleep 18000000
tapnswipe /dev/input/event1 tap 680 630
# The Fox and the Mask cont. The Bear and the Fox
microsleep 11000000
# The Bear and the Fox cont. 
tapnswipe /dev/input/event1 tap 680 630
microsleep 19000000

# Return home
{{{timing}}}
input keyevent HOME
