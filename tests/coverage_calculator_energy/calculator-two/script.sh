#
# Calculator Test (None)
#	Loads a calculator app and does some sample calculations
#
# Copyright (c) 2013 Jed Barlow, Kent Rasmussen, Alex Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundationeither version 2 of the Licenseor
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If notsee <http://www.gnu.org/licenses/>.
#

# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
am start -n com.android2.calculator3/com.android2.calculator3.Calculator
microsleep 6000000

### random ops
{{{timing}}}

###### to avoid initial window for most apps
tapnswipe /dev/input/event1 tap 590 1107 300
microsleep 2000000
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 50 1150 665 1150 2000
microsleep 4000000
###### key event ##########
input keyevent ENTER
microsleep 4000000
###### input text ##########
input text 45
microsleep 4000000
input keyevent ENTER
microsleep 4000000
###### tap ##########
tapnswipe /dev/input/event1 tap 166 585 300
microsleep 4000000
###### tap ##########
tapnswipe /dev/input/event1 tap 62 54 300
microsleep 4000000
###### long press ##########
tapnswipe /dev/input/event1 tap 234 951 1000
microsleep 4000000
###### long press ##########
tapnswipe /dev/input/event1 tap 186 727 1000
microsleep 4000000
###### key event ##########
input keyevent DEL
microsleep 4000000
###### long press ##########
tapnswipe /dev/input/event1 tap 449 278 1000
microsleep 4000000
###### tap menu ##########
tapnswipe /dev/input/event1 tap 682 1236 300
microsleep 3000000
###### tap ##########
tapnswipe /dev/input/event1 tap 448 857 300
microsleep 4000000
###### swipe ##########
tapnswipe /dev/input/event1 tap 665 1150 2000
microsleep 4000000
###### tap menu ##########
tapnswipe /dev/input/event1 tap 682 1236 300
microsleep 3000000
###### tap ##########
tapnswipe /dev/input/event1 tap 583 143 300
microsleep 4000000
###### input text ##########
input text 45
microsleep 4000000
input keyevent ENTER
microsleep 4000000

# "Exit" Process
{{{timing}}}
microsleep 2000000
tapnswipe /dev/input/event1 tap 339 1240
