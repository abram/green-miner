# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.polar.android.mlmidlandmirror/com.polar.android.mlmidlandmirror.activities.PMLauncherActivity
microsleep 10000000
{{{timing}}}
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 100
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 341 647
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 656 916
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 543 671
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 441 1051
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 124 1055
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 39 582
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 28 933
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 191 906
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 166 935
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 613 155
microsleep 2000000
###### input text ##########
input text 45
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 589 142
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 567 358
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 155 424
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 676 725
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 496 859
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 642 366
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 20 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 246 110
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 616 708
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 1100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 659 341
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 20 1000
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME