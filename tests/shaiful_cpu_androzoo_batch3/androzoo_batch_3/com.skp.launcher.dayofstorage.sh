# Wait for Wattlog
microsleep 10000000
# Load App
{{{timing}}}
am start -n com.skp.launcher.dayofstorage/com.skp.theme.sample.MasterActivity
microsleep 10000000
{{{timing}}}
### Prefix to start the app
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent 61
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
tapnswipe /dev/input/event1 tap 200 400
microsleep 2000000
tapnswipe /dev/input/event1 swipe 300 500 300 500
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 100
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 486 733
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 364 965
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 341 395
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 480 910
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### input text ##########
input text hello%sworld
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 516 533
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 110 1005
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 20 100
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 481 918
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 289 592
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### tap menu ##########
input keyevent 82
microsleep 2000000
input keyevent ENTER
microsleep 2000000
input keyevent ENTER
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 277 127
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### key event ##########
input keyevent ENTER
microsleep 2000000
###### key event ##########
input keyevent DEL
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 303 782
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 20 100 695 100
microsleep 2000000
###### tap menu without enter##########
input keyevent 82
microsleep 2000000
###### swipe ##########
tapnswipe /dev/input/event1 swipe 695 1100 695 1000
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 405 683
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 544 506
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 693 805
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 506 890
microsleep 2000000
###### tap ##########
tapnswipe /dev/input/event1 tap 323 1092
microsleep 2000000
### Exit Process
{{{timing}}}
microsleep 2000000
input keyevent HOME