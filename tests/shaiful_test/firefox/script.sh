# Wait for Wattlog
microsleep 10000000

# Load App
{{{timing}}}
am start -n org.mozilla.firefox/.App
microsleep 8000000


{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "http://pizza.cs.ualberta.ca:1800"
tapnswipe /dev/input/event1 tap 667 100
microsleep 5000000
{{{timing}}}
microsleep 10000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "http://pizza.cs.ualberta.ca:1800"
microsleep 4000000
tapnswipe /dev/input/event1 tap 667 100
microsleep 5000000
tapnswipe /dev/input/event1 tap 200 400 50
microsleep 5000000
tapnswipe /dev/input/event1 tap 400 400 50
microsleep 5000000
tapnswipe /dev/input/event1 tap 667 250
{{{timing}}}
microsleep 5000000
{{{timing}}}
tapnswipe /dev/input/event1 tap 400 100 50
tapnswipe /dev/input/event1 tap 400 100 50
microsleep 2500000
input text "https://pizza.cs.ualberta.ca:1801"
microsleep 3000000
tapnswipe /dev/input/event1 tap 667 119
microsleep 3000000
tapnswipe /dev/input/event1 tap 530 333
microsleep 10000000

{{{timing}}}
microsleep 5000000

# "Exit" Process
{{{timing}}}
microsleep 2500000
{{{timing}}}
tapnswipe /dev/input/event1 tap 339 1240

