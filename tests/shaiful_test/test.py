import libgreenminer

class Test(libgreenminer.AndroidTest):
    def before(self, run):
        # Set Screen Timeout
        self.timeout = run.phone.shell('settings get system screen_off_timeout').strip()
        run.phone.shell('settings put system screen_off_timeout 1800000').strip()
        run.phone.adb("sh -c /data/local/strc2.sh&")

    def after(self, run):
        # Reset Screen Timeout
        run.phone.adb("pull /data/local/trc.txt /home/pi/green-star/" + run.version + ".txt")
        run.phone.shell("rm /data/local/trc.txt")
        run.phone.shell('settings put system screen_off_timeout ' + self.timeout)
