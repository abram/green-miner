logcat > /sdcard/logcat.txt &
PID=$!
# Wait for wattlog (10s)
microsleep 10000000

# Load app
{{{timing}}}
am start -n com.example.andrea.wekaandroidport/.MainActivity
# Choose IBk
tapnswipe /dev/input/event1 tap 150 350
microsleep 2000000

# Read data
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 120
microsleep 83000000

# Train CV
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 360
microsleep 60000000

# Validate CV
{{{timing}}}
tapnswipe /dev/input/event1 tap 360 600
microsleep 10000000

# Idle time
{{{timing}}}
kill $PID
