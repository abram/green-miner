# Wait for wattlog
microsleep 7000000

#  now we run all the tests
{{timing}}
{{#TEST_PACKAGES}}
    am instrument -w {{TEST_PACKAGE}}/android.test.InstrumentationTestRunner
{{/TEST_PACKAGES}}

microsleep 12000000

# idle time 
{{timing}}
